/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reservation;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author iulia
 */
public class Reservation {

    private int id = -1;
    private String date;
    private String userEmail;
    private int eventId;
    private int ticketsReserved;

    public Reservation() {
    }

    public Reservation(String userEmail, int eventId, int ticketsReserved) {
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(Calendar.getInstance().getTime());
        this.date = date;
        this.userEmail = userEmail;
        this.eventId = eventId;
        this.ticketsReserved = ticketsReserved;
    }

    public int getId() {
        return id;
    }

    public String getDate() {
        return date;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public int getEventId() {
        return eventId;
    }

    public int getTicketsReserved() {
        return ticketsReserved;
    }

    //CRUD
    // CREATE
    public static boolean insert(Reservation reservation) {
        ReservationGateway.insertReservation(java.sql.Date.valueOf(reservation.date),
                reservation.userEmail,
                reservation.eventId,
                reservation.ticketsReserved);
        return true;
    }

    //READ
    public static ArrayList<Reservation> findByUserEmail(String userEmail) {
        ArrayList<Reservation> reservationList = new ArrayList();
        ResultSet rs;
        rs = ReservationGateway.findReservationByUserEmail(userEmail);
        try {
            while (rs.next()) {
                Reservation reservation = new Reservation();
                reservation.id = rs.getInt("reservation_id");
                reservation.date = rs.getString("reservation_date");
                reservation.userEmail = rs.getString("user_email");
                reservation.eventId = rs.getInt("event_id");
                reservation.ticketsReserved = rs.getInt("reservation_tickets_reserved");

                reservationList.add(reservation);
            } 
            return reservationList;

        } catch (SQLException ex) {
            Logger.getLogger(Reservation.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static Reservation findByUserEmailAndEventId(String userEmail, int eventId) {
        Reservation reservation = new Reservation();
        ResultSet rs;
        rs = ReservationGateway.findReservationByUserEmailAndEventId(userEmail, eventId);
        try {
            if (rs.next()) {
                reservation.id = rs.getInt("reservation_id");
                reservation.date = rs.getString("reservation_date");
                reservation.userEmail = rs.getString("user_email");
                reservation.eventId = rs.getInt("event_id");
                reservation.ticketsReserved = rs.getInt("reservation_tickets_reserved");

                return reservation;
            } else {
                return null;
            }

        } catch (SQLException ex) {
            Logger.getLogger(Reservation.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static boolean addTickets(Reservation reservation, int tickets){
        return ReservationGateway.updateReservation(reservation.id, 
                java.sql.Date.valueOf(reservation.date), 
                reservation.userEmail, 
                reservation.eventId, 
                reservation.ticketsReserved + tickets);
    }
    
    public static boolean setTickets(Reservation reservation, int tickets){
        return ReservationGateway.updateReservation(reservation.id, 
                java.sql.Date.valueOf(reservation.date), 
                reservation.userEmail, 
                reservation.eventId, 
                tickets);
    }

    // DELETE
    public static boolean delete(Reservation reservation) {        
        ReservationGateway.deleteReservation(reservation.id);
        return true;
    }
}
