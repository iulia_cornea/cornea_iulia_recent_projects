/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reservation;

import DataBase.DBConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author iulia
 */
public class ReservationGateway {
    private static final Connection con = DBConnection.getConnection();
    private static Statement stmt;
    private static ResultSet rs;
    
    public static boolean insertReservation(Date date, String userEmail, int eventId, int ticketsReserved) {
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT MAX(reservation_id) FROM reservation");
            rs.next();
            int id = rs.getInt(1);
            id++;
            return stmt.execute("INSERT INTO reservation " + 
                                "VALUES ( " +
                                id + ',' +
                                '\'' + date + '\'' + ',' +
                                '\'' + userEmail + '\'' + ',' +
                                eventId + ',' +
                                ticketsReserved + ')' );
        } catch (SQLException ex) {
            Logger.getLogger(ReservationGateway.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
       
    }
    
    public static ResultSet findReservationByUserEmail(String userEmail){
        
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM reservation WHERE " +
                    " user_email = " + '\'' + userEmail + '\'');
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ReservationGateway.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static ResultSet findReservationByUserEmailAndEventId(String userEmail, int eventId){
        
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM reservation WHERE " +
                    " user_email = " + '\'' + userEmail + '\'' + " AND " +
                    " event_id = " + eventId);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(ReservationGateway.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    static boolean updateReservation(int id, Date date, String userEmail, int eventId, int ticketsReserved){
        try {
            stmt = con.createStatement();
            stmt.execute("UPDATE reservation SET " +
                        " reservation_date = " + '\'' + date + '\'' + ',' +
                        " user_email  = " + '\'' + userEmail + '\'' + ',' +
                        " event_id = " + eventId + ',' + 
                        " reservation_tickets_reserved = " + ticketsReserved +
                        " WHERE reservation_id = " + id);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ReservationGateway.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }

    static boolean deleteReservation(int id) {
        try {
            stmt = con.createStatement();
            stmt.execute("DELETE FROM reservation WHERE reservation_id = " + id);
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(ReservationGateway.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
}
