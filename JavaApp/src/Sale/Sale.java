/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sale;

import Event.EventGateway;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author iulia
 */
public class Sale {
    private int id = -1;
    private String userEmail;
    private int eventId;
    private int ticketsBought;
    private String date;

    public int getId() {
        return id;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public int getEventId() {
        return eventId;
    }

    public int getTicketsBought() {
        return ticketsBought;
    }

    public String getDate() {
        return date;
    }
    
    public Sale(){}
    
    public Sale(int id, String userEmail, int eventId, 
            int ticketsBought, String date){
        this.id = id;
        this.userEmail = userEmail;
        this.eventId = eventId;
        this.ticketsBought = ticketsBought;
        this.date = date;
    }
    
    public Sale( String userEmail, int eventId, 
            int ticketsBought, String date){
        this.userEmail = userEmail;
        this.eventId = eventId;
        this.ticketsBought = ticketsBought;
        this.date = date;
    }
    
    //CRUD 
    public static boolean insert(Sale sale){
        SaleGateway.insertSale(sale.userEmail, sale.eventId, 
                sale.ticketsBought, java.sql.Date.valueOf(sale.date));
        return  true;
    }
    
    public static Sale findById(int id){
        ResultSet rs;
        rs = SaleGateway.findSaleById(id);
        try {
            if(rs.next()){
                Sale sale = new Sale();
                sale.id = rs.getInt("sale_id");
                sale.userEmail = rs.getString("user_email");
                sale.eventId = rs.getInt("event_id");
                sale.ticketsBought = rs.getInt("sale_tickets_bought");
                sale.date = rs.getString("sale_date");
                return sale;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(Sale.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static ArrayList<Sale> findByUserEmail(String userEmail){
        ArrayList<Sale> salesList = new ArrayList();
        ResultSet rs;
        rs = SaleGateway.findSaleByUserEmail(userEmail);
        try {
            while(rs.next()){
                Sale sale = new Sale();
                sale.id = rs.getInt("sale_id");
                sale.userEmail = rs.getString("user_email");
                sale.eventId = rs.getInt("event_id");
                sale.ticketsBought = rs.getInt("sale_tickets_bought");
                sale.date = rs.getString("sale_date");
                salesList.add(sale);
            }
            return salesList;
        } catch (SQLException ex) {
            Logger.getLogger(Sale.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static ArrayList<Sale> findByEventId(int eventId){
        ArrayList<Sale> salesList = new ArrayList();
        ResultSet rs;
        rs = SaleGateway.findSalesByEventId(eventId);
        try {
            while(rs.next()){
                Sale sale = new Sale();
                sale.id = rs.getInt("sale_id");
                sale.userEmail = rs.getString("user_email");
                sale.eventId = rs.getInt("event_id");
                sale.ticketsBought = rs.getInt("sale_tickets_bought");
                sale.date = rs.getString("sale_date");
                salesList.add(sale);
            }
            return salesList;
        } catch (SQLException ex) {
            Logger.getLogger(Sale.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static ArrayList<Sale> findByDate(String date){
        ArrayList<Sale> salesList = new ArrayList();
        ResultSet rs;
        rs = SaleGateway.findSalesByDate(java.sql.Date.valueOf(date));
        try {
            while(rs.next()){
                Sale sale = new Sale();
                sale.id = rs.getInt("sale_id");
                sale.userEmail = rs.getString("user_email");
                sale.eventId = rs.getInt("event_id");
                sale.ticketsBought = rs.getInt("sale_tickets_bought");
                sale.date = rs.getString("sale_date");
                salesList.add(sale);
            }
            return salesList;
        } catch (SQLException ex) {
            Logger.getLogger(Sale.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static ArrayList<Sale> findByUserEmailAndEventId(String userEmail, int eventId){
        ArrayList<Sale> salesList = new ArrayList();
        ResultSet rs;
        rs = SaleGateway.findSalesByUserEmailAndEventId(userEmail, eventId);
        try {
            while(rs.next()){
                Sale sale = new Sale();
                sale.id = rs.getInt("sale_id");
                sale.userEmail = rs.getString("user_email");
                sale.eventId = rs.getInt("event_id");
                sale.ticketsBought = rs.getInt("sale_tickets_bought");
                sale.date = rs.getString("sale_date");
                salesList.add(sale);
            }
            return salesList;
        } catch (SQLException ex) {
            Logger.getLogger(Sale.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static ArrayList<Sale> getAll(){
        ArrayList<Sale> salesList = new ArrayList();
        ResultSet rs;
        rs = SaleGateway.getAllSales();
        try {
            while(rs.next()){
                Sale sale = new Sale();
                sale.id = rs.getInt("sale_id");
                sale.userEmail = rs.getString("user_email");
                sale.eventId = rs.getInt("event_id");
                sale.ticketsBought = rs.getInt("sale_tickets_bought");
                sale.date = rs.getString("sale_date");
                salesList.add(sale);
            }
            return salesList;
        } catch (SQLException ex) {
            Logger.getLogger(Sale.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
}
