/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Sale;

import DataBase.DBConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author iulia
 */
public class SaleGateway {
    private static final Connection con = DBConnection.getConnection();
    private static Statement stmt;
    private static ResultSet rs;
    
    //CRUD with DB interraction
    public static boolean insertSale(String userEmail, int eventId,
            int ticketsBought, Date date){
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT MAX(sale_id) FROM sale");            
            rs.next();            
            int id = rs.getInt(1);
            id++;
            System.out.print("Sale" +"\t" + "Id: " + id  +"\t"
                                           + "Email: " + userEmail  +"\t"
                                           + "Event Id: " + eventId +"\t"
                                           + "Tickets: " + ticketsBought +"\t"
                                           +"Date" + date + '\n');
             
             //stmt.execute("insert into sale values (12, 'user2@domain.com', 6, 3, '2015-09-12')");
            return stmt.execute("INSERT INTO sale " +
                                "VALUES (" +
                                id + ',' +                                
                                eventId + ',' +
                                '\'' + userEmail + '\'' + ',' +
                                '\'' + date + '\'' + ',' +
                                ticketsBought + ')');
        } catch (SQLException ex) {
            Logger.getLogger(SaleGateway.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public static ResultSet findSaleById(int id){
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * from sale WHERE sale_id =" + id);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(SaleGateway.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
    }
    
    public static ResultSet findSalesByEventId(int eventId){
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * from sale WHERE event_id =" + eventId);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(SaleGateway.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
    }
    
    public static ResultSet findSaleByUserEmail(String userEmail){
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * from sale WHERE user_email =" +
                                    '\'' + userEmail + '\'');
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(SaleGateway.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
    }
    
    public static ResultSet findSalesByDate(Date date){
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * from sale WHERE sale_date =" +
                                    '\'' + date + '\'');
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(SaleGateway.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
    }
    
    public static ResultSet findSalesByUserEmailAndEventId(String userEmail, int id){
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * from sale WHERE" + 
                                    " event_id =" + id + " AND " +
                                    " user_email =" + '\'' + userEmail + '\'');
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(SaleGateway.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
        
    }
    
    public static ResultSet getAllSales(){
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM sale");
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(SaleGateway.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    //NO UPDATE ALLOWED YET
    
    //NO DESTROY ALLWOED YET
}
