/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Event;

import DataBase.DBConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author iulia
 */
public class EventGateway {
    private static final Connection con = DBConnection.getConnection();  
    private static  Statement stmt;  
    private static ResultSet rs;
    
    //CRUD with DB interaction
    public static boolean insertEvent(String name, String description, 
            Date date, String location,int noOfTickets, float ticketPrice){
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT MAX(event_id) FROM event");
            rs.next();
            int id = rs.getInt(1);
            id++;
            stmt.execute("INSERT INTO event " +
                                    "VALUES ( " +
                                    id + ',' +
                                    '\'' + name + '\'' + ',' +
                                    '\'' + description + '\'' + ',' +
                                    '\'' + date + '\'' + ',' +
                                    '\'' + location + '\'' + ',' +
                                    noOfTickets + ',' +
                                    0 + ',' +            // reserved tickets
                                    noOfTickets + ',' +   // available tickets
                                    ticketPrice + ')');
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(EventGateway.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        
    }
    
    public static ResultSet findEvent(int id){        
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT  * from event WHERE event_id =" + id);
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(EventGateway.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }        
    }
    
    public static ResultSet findEvents(String name){        
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT  * from event WHERE event_name =" + '\'' + name + '\'');
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(EventGateway.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }        
    }
    
    public static ResultSet getAllEvents(){        
        try {
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM event");
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(EventGateway.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }        
    }
    
    public static boolean updateEvent(int id, String name, String description, 
            Date date, String location,int noOfTickets, int reservedTickets, 
            int availableTickets, float ticketPrice){        
        try {
            stmt = con.createStatement();
            stmt.execute("UPDATE event SET "+
                                "event_name = " + '\'' + name + '\'' + ',' +
                                "event_description = " + '\'' + description + '\'' + ',' +
                                "event_date = " + '\'' + date + '\'' + ',' +
                                "event_location = " + '\'' + location + '\'' + ',' +
                                "event_no_of_tickets = " + noOfTickets + ',' +
                                "event_reserved_tickets = " + reservedTickets + ',' +
                                "event_available_tickets = " + availableTickets + ',' +
                                "event_ticket_price = " + ticketPrice +
                                " WHERE event_id ="  + id); 
            return  true;
        } catch (SQLException ex) {
            Logger.getLogger(EventGateway.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }    
    
    public static boolean deleteEvent(int id){
        try {
            stmt = con.createStatement();
            stmt.execute("DELETE FROM event WHERE event_id =" + id);
            return  true;
        } catch (SQLException ex) {
            Logger.getLogger(EventGateway.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
}
