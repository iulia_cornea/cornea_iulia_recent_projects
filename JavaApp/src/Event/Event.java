/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Event;


import java.util.ArrayList;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author iulia
 */
public class Event {
    private int id = -1;
    private String name;
    private String description;
    private String date;
    private String location;
    private int noOfTickets;
    private int reservedTickets;
    private int availableTickets;
    private float ticketPrice;
    
    //public static 
    public Event(){}
    
    private Event(int id, String name, String description, String date, 
            String location, int noOfTickets, float ticketPrice){
        this.id = id;
        this.name = name;
        this.description = description;
        this.date = date;
        this.location = location;
        this.noOfTickets = noOfTickets;
        this.reservedTickets = 0;
        this.availableTickets = noOfTickets;
        this.ticketPrice = ticketPrice;
    }
    
    public Event( String name, String description, String date, 
            String location, int noOfTickets, float ticketPrice){
        this.name = name;
        this.description = description;
        this.date = date;
        this.location = location;
        this.noOfTickets = noOfTickets;
        this.reservedTickets = 0;
        this.availableTickets = noOfTickets;        
        this.ticketPrice = ticketPrice;
    }
    
    public int getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getDate() {
        return date;
    }

    public String getLocation() {
        return location;
    }

    public int getNoOfTickets() {
        return noOfTickets;
    }

    public int getReservedTickets() {
        return reservedTickets;
    }

    public int getAvailableTickets() {
        return availableTickets;
    }
        
    public float getTicketPrice() {
        return ticketPrice;
    }
    
    //CRUD methods based on Event param
    //CREATE
    public static boolean insert(Event event){
        EventGateway.insertEvent(event.name, event.description,
                java.sql.Date.valueOf(event.date), event.location,event.noOfTickets, event.ticketPrice);
        return true;
    }
    //FIND 1
    public static Event find(int id){
        Event event = new Event();
        ResultSet rs = EventGateway.findEvent(id);
        try {
            if(rs.next()){
                event.id = rs.getInt("event_id");
                event.name = rs.getString("event_name");
                event.description = rs.getString("event_description");
                event.date = rs.getString("event_date");
                event.location = rs.getString("event_location");
                event.noOfTickets = rs.getInt("event_no_of_tickets");
                event.reservedTickets = rs.getInt("event_reserved_tickets");
                event.availableTickets = rs.getInt("event_available_tickets");
                event.ticketPrice = rs.getFloat("event_ticket_price");
                return event;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(Event.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }        
    }
    
    public static ArrayList<Event> getAll(){
        ArrayList<Event> eventList= new ArrayList();
        try {
            ResultSet rs = EventGateway.getAllEvents();
            while(rs.next()){
                Event event = new Event();
                event.id = rs.getInt("event_id");
                event.name = rs.getString("event_name");
                event.description = rs.getString("event_description");
                event.date = rs.getString("event_date");
                event.location = rs.getString("event_location");
                event.noOfTickets = rs.getInt("event_no_of_tickets");
                event.reservedTickets = rs.getInt("event_reserved_tickets");
                event.availableTickets = rs.getInt("event_available_tickets");
                event.ticketPrice = rs.getFloat("event_ticket_price");
                eventList.add(event);                
            }
            return eventList;
        } catch (SQLException ex) {
            Logger.getLogger(Event.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }        
    }
    //UPDATE
    public static boolean update(Event event, String name, String description,
                    String date, String location, int noOfTickets, 
                    int reservedTickets, int availableTickets, float ticketPrice){
        if(find(event.id) == null){
            return false;
        }
        EventGateway.updateEvent(event.id, name, description,
                    java.sql.Date.valueOf(date), location, noOfTickets, 
                    reservedTickets, availableTickets, ticketPrice);
        return true;
    }
    //DELETE
    public static boolean delete(Event event){
        if(find(event.id) == null){
            return false;
        }
        EventGateway.deleteEvent(event.id);
         return true;
    }

    
}  

