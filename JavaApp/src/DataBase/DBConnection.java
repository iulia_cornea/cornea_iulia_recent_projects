/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DataBase;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author iulia
 */
public class DBConnection {
    private static boolean instance = false;
    private static Connection con;
    private static final String host = "jdbc:mysql://localhost:3306/TicketDB";
    private static final String user = "root";
    private static final String pass = "";
    
    
    private DBConnection () {
    }
    
    private static Connection connect(){
       if (!instance){
            instance = true;
                
            try {
                Class.forName("com.mysql.jdbc.Driver");
                con = DriverManager.getConnection( host, user, pass );
            } catch (SQLException ex) {
                System.out.print(ex.getMessage());
            }catch(ClassNotFoundException e){
                e.printStackTrace();
            }
            
            return con;
       }
       else{
           return con;
       }
    }
    
     public static Connection getConnection(){
        return connect();
    }
        
}
