/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package User;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author iulia
 */
public class User {
    private int type;
    private String registrationDate;
    private String email;
    private String password;
    private String picturePath;
    private int ticketsBought;
    
    
    public User(){
    }
    
    public User(int type, String email, String password, 
            String picturePath, int ticketsBought){
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(Calendar.getInstance().getTime());
        this.registrationDate = date;
        this.type = type;
        this.email = email;
        this.password = password;
        this.picturePath = picturePath;
        this.ticketsBought = ticketsBought;
    }
     
    public int getType(){
        return type;
    }
    
    public String getRegistrationDate() {
        return registrationDate;
    }
    
    public String getEmail(){
        return email;
    }
    
    public String getPassword(){
        return password;
    }  
    
    public String getPicturePath() {
        return picturePath;
    }

    public int getTicketsBought() {
        return ticketsBought;
    }
     
    
    //CRUD methods based on User param
    //CREATE
    public static boolean insert(User user){
        if(find(user.email) == null){
            UserGateway.insertUser(user.type, java.sql.Date.valueOf(user.registrationDate), 
                                    user.email, user.password, user.picturePath);
            return  true;
        }        
        return false;
    }
    //READ 1
    public static User find(String email){
        User user = new User();
        ResultSet rs = UserGateway.findUser(email);
        try {
            if (rs.next()){
                user.type = rs.getInt("user_type");
                user.registrationDate = rs.getString("user_registration_date");
                user.email = rs.getString("user_email");
                user.password = rs.getString("user_pass");
                user.picturePath = rs.getString("user_picture_path");
                user.ticketsBought = rs.getInt("user_tickets_bought");
                return user;
            }
            return null;
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }        
    }
    //READ all
    public static ArrayList<User> getAll(){
        ArrayList<User> userList = new ArrayList();
        try {
            ResultSet rs;
            rs = UserGateway.getAllUsers();
            // .next() points just before the first row
            while(rs.next()){
                User user = new User();
                user.type = rs.getInt("user_type");
                user.registrationDate = rs.getString("user_registration_date");
                user.email = rs.getString("user_email");
                user.password = rs.getString("user_pass");
                user.picturePath = rs.getString("user_picture_path");
                user.ticketsBought = rs.getInt("user_tickets_bought");
                userList.add(user);
            }
             return userList;
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }    
    //UPDATE
    public static boolean update(User user, int newType, String newPass, 
                                String newPicturePath, int newTicketsBought){
        if(find(user.email) == null){
            return false;
        }
        UserGateway.updateUser(user.email, newType, newPass, newPicturePath, newTicketsBought);
        return  true;
    }
    //DELETE
    public static boolean delete(User user){
        if(find(user.email) == null){                
            return false;
        }
        UserGateway.deleteUser(user.email);
        return  true;        
    }
    
    //Non - CRUD methods based on non user object
    //login 
    public static boolean login(String email, String pass){
        ResultSet rs = UserGateway.findUser(email);
        try {
            if(!rs.next()){
                return false;
            }
            if(!rs.getString("user_pass").equals(pass)){
                return false;
            }
            //user exists and password matches
            return true;
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
            
    }
    //change password
    public static boolean changePassword(User user, String newPass){
        if(User.find(user.email) == null)
            return false;
        User.update(user, user.type, newPass, user.picturePath, user.ticketsBought);
        return  true;
    }
    
    //change photo
    public static boolean changePicture(User user, String newPicturePath){
        if(User.find(user.email) == null)
            return false;
        User.update(user, user.type, user.password, newPicturePath, user.ticketsBought);
        return  true;
    }
    
}
