/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package User;

import DataBase.DBConnection;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author iulia
 */
public class UserGateway {
    private static final Connection con = DBConnection.getConnection();    
    private static ResultSet rs;
    private static Statement stmt;
    
    UserGateway (){
        
    }
    
    public static boolean insertUser(int type, Date date, String email, 
                                    String pass,String picturePath){
        try {
            stmt = con.createStatement( );
            stmt.execute("INSERT INTO user " +
                                    "VALUES ( " +
                                    type + ',' +
                                    '\'' + date + '\'' + ',' +
                                    '\'' + email + '\'' + ',' +
                                    '\'' + pass+ '\'' + ',' +
                                    '\'' + picturePath + '\'' + ',' +
                                    0  +')');
            return  true;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
    }
    
    public static ResultSet getAllUsers(){
        try {
            stmt = con.createStatement( );
            rs = stmt.executeQuery("SELECT * from user");
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
    
    public static ResultSet findUser(String email){
        try {
            stmt = con.createStatement( );
            rs = stmt.executeQuery("SELECT * FROM user WHERE user_email = " +
                                    '\'' + email + '\'');
            return rs;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }
   
    public static boolean updateUser(String email, int newType,  String newPass,
                                        String newPicturePath, int newTicketsBought){
        try {
            stmt = con.createStatement();
            stmt.execute("UPDATE user SET "+
                            "user_type = " + newType + ',' +
                            "user_pass = " + '\'' + newPass + '\'' + ',' +
                            "user_picture_path = " + '\'' + newPicturePath + '\'' + ',' +
                            "user_tickets_bought = " + newTicketsBought +
                            " WHERE user_email =" + '\'' + email + '\''); 
            return  true;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
     }
    
    public static boolean deleteUser(String email){
        try {
            stmt = con.createStatement();
            stmt.execute("DELETE from user WHERE user_email = " + '\'' + email + '\'');     
            return  true;
        } catch (SQLException ex) {
            Logger.getLogger(DBConnection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
     }
}
