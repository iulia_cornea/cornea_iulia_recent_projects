/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Sale.Sale;
import User.User;
import java.util.ArrayList;

/**
 *
 * @author iulia
 */
public class SaleView {
    public static void print(ArrayList<Sale> userList){
        Sale u;
        for(int i = 0; i< userList.size(); i++){
            u = userList.get(i);
            System.out.print("Sale" +"\t" + "Id: " + u.getId()  +"\t"
                                           + "Email: " + u.getUserEmail()  +"\t"
                                           + "Tickets: " + u.getTicketsBought()  +"\t"
                                           + "EventId: " + u.getEventId() + "\t"
                                           + "Date" + u.getDate() + '\n');
        }
    }
    
    public static void print(Sale u){
        
        System.out.print("Sale" +"\t" + "Id: " + u.getId()  +"\t"
                                           + "Email: " + u.getUserEmail()  +"\t"
                                           + "Tickets: " + u.getTicketsBought()  +"\t"
                                           + "EventId: " + u.getEventId() + "\t"
                                           + "Date" + u.getDate() + '\n');
    }
}

