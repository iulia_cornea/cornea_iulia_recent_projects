/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import Event.Event;
import Sale.Sale;
import java.io.IOException;
import java.sql.Date;
import java.util.ArrayList;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author iulia
 */
public class FXMLVisitorPageController implements Initializable {

    @FXML
    private TableView<Event> table;
    
    @FXML
    private TableColumn cName;
    
    @FXML
    private TableColumn cDescription;
    
    @FXML
    private TableColumn cLocation;
    
    @FXML
    private TableColumn cDate;
    
    @FXML
    private TableColumn cTicketsAvailable;
    
    @FXML
    private TableColumn cTicketPrice;
    
    private final ArrayList<Sale> sales = Sale.getAll();
    private final ObservableList<Sale> oSales = FXCollections.observableArrayList(sales);
    
    private final ArrayList<Event> events = Event.getAll();
    private final ObservableList<Event> oEvents = FXCollections.observableArrayList(events);
    
    @FXML
    private void showEvents(){
        
        cName.setCellValueFactory(new PropertyValueFactory<Event,String>("name"));
        cDescription.setCellValueFactory(new PropertyValueFactory<Event,String>("description"));
        cLocation.setCellValueFactory(new PropertyValueFactory<Event,String>("location"));
        cDate.setCellValueFactory(new PropertyValueFactory<Event,Date>("date"));
        cTicketsAvailable.setCellValueFactory(new PropertyValueFactory<Event,Integer>("noOfTickets"));
        cTicketPrice.setCellValueFactory(new PropertyValueFactory<Event,Float>("ticketPrice"));
                
        table.setItems(oEvents);
                  
    }
    
    @FXML
    private void goToLogin(ActionEvent event) throws IOException{
        Parent user_login_parent = FXMLLoader.load(getClass().getResource("FXMLLogin.fxml"));
        Scene user_login_scene = new Scene(user_login_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(user_login_scene);
        app_stage.show();
    }
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        showEvents();
    }    
    
}
