/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author iulia
 */
public class FXMLAdminPageController implements Initializable {

    @FXML
    private void logOut(ActionEvent event) throws IOException {
        Test.currentUser = null;
        Parent user_login_parent = FXMLLoader.load(getClass().getResource("FXMLLogin.fxml"));
        Scene user_login_scene = new Scene(user_login_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(user_login_scene);
        app_stage.show();
    }

    @FXML
    private void goToAdminEvents(ActionEvent event) throws IOException {
        Parent admin_events_parent = FXMLLoader.load(getClass().getResource("FXMLAdminEvents.fxml"));
        Scene admin_events_scene = new Scene(admin_events_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(admin_events_scene);
        app_stage.show();

    }
    
    @FXML
    private void goToAdminReports(ActionEvent event) throws IOException {
        Parent admin_reports_parent = FXMLLoader.load(getClass().getResource("FXMLAdminReports.fxml"));
        Scene admin_reports_scene = new Scene(admin_reports_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(admin_reports_scene);
        app_stage.show();

    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
