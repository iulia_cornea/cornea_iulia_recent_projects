/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author sky
 */
public class ModelFXMLController implements Initializable {
     
    @FXML
    private Button btn;
    
    @FXML
    private Label lbl;
    @FXML
    private Label lbl2;
    
    @FXML
    private TextField tf;
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        lbl.setVisible(false);
    }   
   
    
    @FXML
    void btnApasat(){
        lbl.setVisible(true);
        lbl2.setText(tf.getText());
    }
    
}
