/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import User.User;
import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
/**
 *
 * @author iulia
 */
public class FXMLLoginController implements Initializable {

    @FXML
    private TextField email;
    @FXML
    private PasswordField pass;
    @FXML
    private Label emailNotFound;
    @FXML
    private Label passNotMatch;
    @FXML
    private Button login;
    @FXML
    private Button signUp;
    @FXML
    private Button visit;

    @FXML
    private void login(ActionEvent event) throws IOException {
        emailNotFound.setVisible(false);
        passNotMatch.setVisible(false);

        if (User.find(email.getText()) == null) {
            emailNotFound.setVisible(true);
            return;
        }
        if (!pass.getText().equals(User.find(email.getText()).getPassword())) {
            passNotMatch.setVisible(true);
            return;
        }

        Test.currentUser = User.find(email.getText());
        if (Test.currentUser.getType() == 0) {
            Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLAdminPage.fxml"));
            Scene home_page_scene = new Scene(home_page_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(home_page_scene);
            app_stage.show();
            return;
        }
        if (Test.currentUser.getType() == 1) {
            Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLUserPage.fxml"));
            Scene home_page_scene = new Scene(home_page_parent);
            Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            app_stage.hide();
            app_stage.setScene(home_page_scene);
            app_stage.show();
            return;
        }

    }

    @FXML
    private void resetPass() {
        emailNotFound.setVisible(false);
        if (User.find(email.getText()) == null) {
            emailNotFound.setVisible(true);
            return;
        }
        Test.currentUser = User.find(email.getText());
        User.changePassword(Test.currentUser, pass.getText());
        Test.currentUser = User.find(Test.currentUser.getEmail());
        sendEmail();

    }

    
    private void sendEmail() {
        SendGrid sendgrid = new SendGrid("iulishkiri", "why2ufo?");

        SendGrid.Email email = new SendGrid.Email();
        email.addTo(Test.currentUser.getEmail());
        email.setFrom("iulia.cornea5@gmail.com");
        email.setSubject("Password Reset");
        email.setText("Your new password is:\n" + Test.currentUser.getPassword());

        try {
            SendGrid.Response response = sendgrid.send(email);
            System.out.println(response.getMessage());
        } catch (SendGridException e) {
            System.err.println(e);
        }
    }


@FXML
        private void signUp(ActionEvent event) throws IOException {
        Parent user_sign_up_parent = FXMLLoader.load(getClass().getResource("FXMLUserSignUp.fxml"));
        Scene user_sign_up_scene = new Scene(user_sign_up_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(user_sign_up_scene);
        app_stage.show();
    }
    
    @FXML
        private void visit(ActionEvent event) throws IOException{
        Parent visitor_page_parent = FXMLLoader.load(getClass().getResource("FXMLVisitorPage.fxml"));
        Scene visitor_page_scene = new Scene(visitor_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(visitor_page_scene);
        app_stage.show();
    }

    @Override
        public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
