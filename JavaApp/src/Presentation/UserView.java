/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import User.User;
import java.util.ArrayList;

/**
 *
 * @author iulia
 */
public class UserView {
    
    public static void print(ArrayList<User> userList){
        User u;
        for(int i = 0; i< userList.size(); i++){
            u = userList.get(i);
            System.out.print("User" +"\t" + "Type: " + u.getType()  +"\t"
                                           + "Email: " + u.getEmail()  +"\t"
                                           + "Password: " + u.getPassword() + '\n');
        }
    }
    
    public static void print(User user){
        
        System.out.print("User" +"\t" + "Type: " + user.getType() +"\t" 
                         + " Email: " + user.getEmail() +"\t" 
                         + " Password: " + user.getPassword() + '\n');
    }
}
