/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Event.Event;
import Reservation.Reservation;
import Sale.Sale;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author iulia
 */
public class FXMLUserSalesController implements Initializable {

    
    @FXML
    private TableView tSales;
    @FXML
    private TableColumn cTicketsBought;
    @FXML
    private TableColumn cSaleDate;
    
    @FXML
    private Label lDetails;
    
    private void showSales() {
        ArrayList<Sale> sales = Sale.findByUserEmail(Test.currentUser.getEmail());
        ObservableList<Sale> oSales = FXCollections.observableArrayList(sales);
        cTicketsBought.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("ticketsBought"));
        cSaleDate.setCellValueFactory(new PropertyValueFactory<Reservation, String>("date"));

        tSales.setItems(oSales);
    }
    
    @FXML
    private void showDetails(){
        lDetails.setVisible(false);
        Sale selectedSale = (Sale) tSales.getSelectionModel().getSelectedItem();
        if(selectedSale == null){
            lDetails.setText("You must first select a sale");
            lDetails.setVisible(true);
            return;
        }
        Event selectedEvent = Event.find(selectedSale.getEventId());
        lDetails.setText("You bought tickets to the " + selectedEvent.getName() 
                + " which takes place at " + selectedEvent.getLocation() 
                + " in " + selectedEvent.getDate());
        lDetails.setVisible(true);
    }
    
    @FXML
    private void goToUserPage(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLUserPage.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(home_page_scene);
        app_stage.show();
    }
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        showSales();
        lDetails.setVisible(false);
    }    
    
}
