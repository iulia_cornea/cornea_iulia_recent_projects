/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import User.User;
import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;
import javax.imageio.ImageIO;

/**
 * FXML Controller class
 *
 * @author iulia
 */
public class FXMLUserPageController implements Initializable {

    @FXML
    private Label userName;
    @FXML
    private Label lpic;
    @FXML
    private TextField tfpic;
    @FXML
    private Button bpic;
    @FXML
    private ImageView ivpic;

    @FXML
    private void logOut(ActionEvent event) throws IOException {
        Test.currentUser = null;
        Parent user_login_parent = FXMLLoader.load(getClass().getResource("FXMLLogin.fxml"));
        Scene user_login_scene = new Scene(user_login_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(user_login_scene);
        app_stage.show();
    }

    @FXML
    private void goToUserEvents(ActionEvent event) throws IOException {
        Parent user_events_parent = FXMLLoader.load(getClass().getResource("FXMLUserEvents.fxml"));
        Scene user_events_scene = new Scene(user_events_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(user_events_scene);
        app_stage.show();

    }

    @FXML
    private void goToUserReservations(ActionEvent event) throws IOException {
        Parent user_events_parent = FXMLLoader.load(getClass().getResource("FXMLUserReservations.fxml"));
        Scene user_events_scene = new Scene(user_events_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(user_events_scene);
        app_stage.show();

    }

    @FXML
    private void goToUserSales(ActionEvent event) throws IOException {
        Parent user_events_parent = FXMLLoader.load(getClass().getResource("FXMLUserSales.fxml"));
        Scene user_events_scene = new Scene(user_events_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(user_events_scene);
        app_stage.show();

    }

    @FXML
    private void prepareToChange() {
        lpic.setVisible(true);
        tfpic.setVisible(true);
        bpic.setVisible(true);
    }

    @FXML
    private void changePicture() {
        //change picture
        String picPath = tfpic.getText();
        User.changePicture(Test.currentUser, picPath);
        Test.currentUser = User.find(Test.currentUser.getEmail());
        lpic.setVisible(false);
        tfpic.setVisible(false);
        bpic.setVisible(false);
        setPic();
    }
    
    private void setPic(){
        try {
            String picPath = Test.currentUser.getPicturePath();
            if(picPath.equals("")){
                picPath = "http://31.media.tumblr.com/tumblr_kwhtgyYpgd1qzscuuo1_400.jpg";
            }
            URL picURL = new URL(picPath);
            Image pic = ImageIO.read(picURL);
            ivpic.setImage(javafx.embed.swing.SwingFXUtils.toFXImage((BufferedImage) pic, null));
        } catch (MalformedURLException ex) {
            Logger.getLogger(FXMLUserPageController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FXMLUserPageController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        userName.setText(Test.currentUser.getEmail());
        lpic.setVisible(false);
        tfpic.setVisible(false);
        bpic.setVisible(false);
        setPic();

    }

}
