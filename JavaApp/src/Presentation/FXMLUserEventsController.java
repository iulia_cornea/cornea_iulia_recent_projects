/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Sale.Sale;
import Event.Event;
import Reservation.Reservation;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Paint;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author iulia
 */
public class FXMLUserEventsController implements Initializable {

    @FXML
    private TableView<Event> table;

    @FXML
    private TableColumn cName;

    @FXML
    private TableColumn cDescription;

    @FXML
    private TableColumn cLocation;

    @FXML
    private TableColumn cDate;

    @FXML
    private TableColumn cTicketsAvailable;

    @FXML
    private TableColumn cTicketPrice;

    @FXML
    private void showEvents() {
        ArrayList<Event> events = Event.getAll();
        ObservableList<Event> oEvents = FXCollections.observableArrayList(events);
        cName.setCellValueFactory(new PropertyValueFactory<Event, String>("name"));
        cDescription.setCellValueFactory(new PropertyValueFactory<Event, String>("description"));
        cLocation.setCellValueFactory(new PropertyValueFactory<Event, String>("location"));
        cDate.setCellValueFactory(new PropertyValueFactory<Event, Date>("date"));
        cTicketsAvailable.setCellValueFactory(new PropertyValueFactory<Event, Integer>("availableTickets"));
        cTicketPrice.setCellValueFactory(new PropertyValueFactory<Event, Float>("ticketPrice"));

        table.setItems(oEvents);

    }

    @FXML
    private Label eventName;
    @FXML
    private Label overReserved;
    @FXML
    private Label overBought;
    @FXML
    private Label successfullyBought;
    @FXML
    private Label noTicketsAvailable;
    @FXML
    private Button reserve;
    @FXML
    private Button buy;
    @FXML
    private TextField noToReserve;
    @FXML
    private TextField noToBuy;

    @FXML
    private void selectEvent() {
        eventName.setTextFill(Paint.valueOf("black"));
        Event selectedEvent = table.getSelectionModel().getSelectedItem();
        if (selectedEvent != null) {
            eventName.setText(selectedEvent.getName());
        }
    }

    @FXML
    private void goToUserPage(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLUserPage.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    private void reserveTickets() {
        overReserved.setVisible(false);
        noTicketsAvailable.setVisible(false);
        eventName.setTextFill(Paint.valueOf("black"));

        // Select an event
        if (table.getSelectionModel().getSelectedItem() == null) {
            eventName.setText("No event Selected yet");
            eventName.setTextFill(Paint.valueOf("red"));
            return;
        }

        // Specify no of tickets
        if (noToReserve.getText().equals("")) {
            eventName.setText("You must specify a no of tickets");
            eventName.setTextFill(Paint.valueOf("red"));
            return;
        }

        selectEvent();
        int ticketsToReserve = Integer.parseInt(noToReserve.getText());
        Event selectedEvent = table.getSelectionModel().getSelectedItem();

        // Can not reserve more than 5
        if (ticketsToReserve > 5) {
            overReserved.setText("The reservation limit is 5");
            overReserved.setVisible(true);
            return;
        }

        // Check if there are enough tickets & ticketsToReserve < 5
        if ((selectedEvent.getAvailableTickets() - ticketsToReserve < 0) || (ticketsToReserve > 5)) {
            noTicketsAvailable.setText("There are only " + selectedEvent.getNoOfTickets() + " tickets available");
            noTicketsAvailable.setVisible(true);
            return;
        }

        // Max 5 resevation for event
        Reservation reservation
                = Reservation.findByUserEmailAndEventId(Test.currentUser.getEmail(), selectedEvent.getId());
        
        if (reservation != null) {
            //reservation already exists            
            if (reservation.getTicketsReserved() + ticketsToReserve > 5) {
                overReserved.setText("You have already reserved "
                        + reservation.getTicketsReserved() + ". The limit is 5!");
                overReserved.setVisible(true);
                return;
            } else {
                Reservation.addTickets(reservation, ticketsToReserve);
                Event.update(selectedEvent, selectedEvent.getName(), selectedEvent.getDescription(),
                selectedEvent.getDate(), selectedEvent.getLocation(),
                selectedEvent.getNoOfTickets(), 
                selectedEvent.getReservedTickets() + ticketsToReserve,
                selectedEvent.getAvailableTickets() - ticketsToReserve,
                selectedEvent.getTicketPrice());
                overReserved.setText("You have successfully reserved "
                        + ticketsToReserve + " more tickets to the event. Now you have "
                        + Reservation.findByUserEmailAndEventId(Test.currentUser.getEmail(), selectedEvent.getId()).getTicketsReserved() 
                        + " tickets reserved");
                overReserved.setVisible(true);
                showEvents();
            }

        } else {
            //make reseravtion
            Reservation.insert(new Reservation(Test.currentUser.getEmail(),
                    selectedEvent.getId(),
                    ticketsToReserve));
             Event.update(selectedEvent, selectedEvent.getName(), selectedEvent.getDescription(),
                selectedEvent.getDate(), selectedEvent.getLocation(),
                selectedEvent.getNoOfTickets(), 
                selectedEvent.getReservedTickets() + ticketsToReserve,
                selectedEvent.getAvailableTickets() - ticketsToReserve,
                selectedEvent.getTicketPrice());
            overReserved.setText("You have successfully reserved "
                    + ticketsToReserve + " tickets to the event.");
            overReserved.setVisible(true);
            showEvents();
        }

    }

    @FXML
    private void buyTickets() {
        successfullyBought.setVisible(false);
        overBought.setVisible(false);
        noTicketsAvailable.setVisible(false);
        eventName.setTextFill(Paint.valueOf("black"));

        // Select an event
        if (table.getSelectionModel().getSelectedItem() == null) {
            eventName.setText("No event Selected yet");
            eventName.setTextFill(Paint.valueOf("red"));
            return;
        }

        // Specify no of tickets
        if (noToBuy.getText().equals("")) {
            eventName.setText("You must specify a no of tickets");
            eventName.setTextFill(Paint.valueOf("red"));
            return;
        }

        selectEvent();
        int ticketsToBuy = Integer.parseInt(noToBuy.getText());
        Event selectedEvent = table.getSelectionModel().getSelectedItem();

        // Check if there are enough tickets
        if (selectedEvent.getAvailableTickets() - ticketsToBuy < 0) {
            noTicketsAvailable.setText("There are only " + selectedEvent.getNoOfTickets() + " tickets available");
            noTicketsAvailable.setVisible(true);
            return;
        }

        ArrayList<Sale> sales = Sale.findByUserEmailAndEventId(Test.currentUser.getEmail(), selectedEvent.getId());
        int ticketsBought = 0;
        for (Sale s : sales) {
            ticketsBought += s.getTicketsBought();
        }

        // Check max 5 tickets for event
        if (ticketsBought + ticketsToBuy > 5) {
            overBought.setText("You have already bought " + ticketsBought
                    + " tickets. The limit is 5!");
            overBought.setVisible(true);
            return;
        }

        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(Calendar.getInstance().getTime());

        Sale.insert(new Sale(Test.currentUser.getEmail(), selectedEvent.getId(),
                ticketsToBuy, date));
        Event.update(selectedEvent, selectedEvent.getName(), selectedEvent.getDescription(),
                selectedEvent.getDate(), selectedEvent.getLocation(),
                selectedEvent.getNoOfTickets(), selectedEvent.getReservedTickets(),
                selectedEvent.getAvailableTickets() - ticketsToBuy,
                selectedEvent.getTicketPrice());

        showEvents();
        successfullyBought.setText("You have successfully bought " + noToBuy.getText()
                + " tickets to the event.");
        successfullyBought.setVisible(true);
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        showEvents();
        overReserved.setVisible(false);
        overBought.setVisible(false);
        successfullyBought.setVisible(false);
        noTicketsAvailable.setVisible(false);
    }

}
