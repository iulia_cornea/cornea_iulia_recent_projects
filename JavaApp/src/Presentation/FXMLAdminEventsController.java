/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Event.Event;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 * FXML Controller class
 *
 * @author iulia
 */
public class FXMLAdminEventsController implements Initializable {

    @FXML
    private TableView<Event> table;

    @FXML
    private TableColumn cName;

    @FXML
    private TableColumn cDescription;

    @FXML
    private TableColumn cLocation;

    @FXML
    private TableColumn cDate;

    @FXML
    private TableColumn cTicketsAvailable;

    @FXML
    private TableColumn cTicketPrice;

    private final ArrayList<Event> events = Event.getAll();
    private final ObservableList<Event> oEvents = FXCollections.observableArrayList(events);

    @FXML
    private void showEvents() {

        cName.setCellValueFactory(new PropertyValueFactory<Event, String>("name"));
        cDescription.setCellValueFactory(new PropertyValueFactory<Event, String>("description"));
        cLocation.setCellValueFactory(new PropertyValueFactory<Event, String>("location"));
        cDate.setCellValueFactory(new PropertyValueFactory<Event, Date>("date"));
        cTicketsAvailable.setCellValueFactory(new PropertyValueFactory<Event, Integer>("availableTickets"));
        cTicketPrice.setCellValueFactory(new PropertyValueFactory<Event, Float>("ticketPrice"));

        table.setItems(oEvents);

    }

    @FXML
    private TextField tfName;
    @FXML
    private TextField tfDescription;
    @FXML
    private TextField tfLocation;
    @FXML
    private TextField tfDate;
    @FXML
    private TextField tfTicketsAvailable;
    @FXML
    private TextField tfTicketPrice;
    @FXML
    private Label successfullyInserted;
    @FXML
    private Label notInserted;

    @FXML
    private void addEvent() {
        successfullyInserted.setVisible(false);
        notInserted.setVisible(false);
        String name = tfName.getText();
        String description = tfDescription.getText();
        String date = tfDate.getText();
        String location = tfLocation.getText();
        int noOfTickets = Integer.parseInt(tfTicketsAvailable.getText());
        float ticketPrice = Float.parseFloat(tfTicketPrice.getText());
        boolean inserted;
        Event newEvent = new Event(name, description, date,
                location, noOfTickets, ticketPrice);
        inserted = Event.insert(newEvent);
        if (inserted) {
            oEvents.add(newEvent);
        }
        successfullyInserted.setVisible(inserted);
        notInserted.setVisible(!inserted);

    }

    @FXML
    private void gotoAdminPage(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLAdminPage.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    private Button bPrepImport;
    @FXML
    private Button bImport;
    @FXML
    private TextField tfImport;
    @FXML
    private Label lImport;

    @FXML
    private void prepareToImport() {
        lImport.setTextFill(Color.BLACK);
        lImport.setText("Write file path");
        bImport.setVisible(true);
        tfImport.setVisible(true);
        lImport.setVisible(true);
    }

    @FXML
    private void importEvent() {
        if (tfImport.getText().equals("")) {
            lImport.setTextFill(Color.RED);
            return;
        }
        
        try {
            File file = new File(tfImport.getText());
            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            if(!doc.getDocumentElement().getNodeName().equals("events")){
                lImport.setText("File is not right format");
                return;
            }
            if(!doc.hasChildNodes()){
                lImport.setText("There are no events to import");
                lImport.setTextFill(Color.BLACK);
                return;
            }
            //#document <events><event>....
            //lista cu toate <event>
            NodeList eventsList = doc.getChildNodes().item(0).getChildNodes();
            String eName = "", eDescription = "", eDate = "", eLocation = "";
            int eTickets = 0;
            float ePrice = 0.0f;
            //must go through each event
            for(int i = 0; i< eventsList.getLength(); i++){
                org.w3c.dom.Node eventNode =  eventsList.item(i);
                if(eventNode.hasChildNodes()){
                    NodeList eventDetails = eventNode.getChildNodes();
                    //must go through each deatail of the event
                    for(int j=0;j<eventDetails.getLength();j++){
                        org.w3c.dom.Node tempNode = eventDetails.item(j);
                        if(tempNode.getNodeName().equals("name")){
                            eName = tempNode.getTextContent();
                        }
                        if(tempNode.getNodeName().equals("description")){
                            eDescription = tempNode.getTextContent();
                        }
                        if(tempNode.getNodeName().equals("date")){
                            eDate = tempNode.getTextContent();
                        }
                        if(tempNode.getNodeName().equals("location")){
                            eLocation= tempNode.getTextContent();
                        }
                        if(tempNode.getNodeName().equals("tickets")){
                            eTickets = Integer.parseInt(tempNode.getTextContent());
                        }
                        if(tempNode.getNodeName().equals("price")){
                            ePrice = Float.parseFloat(tempNode.getTextContent());
                        } 
                        System.out.println(tempNode.getNodeName());
                    }
                    
                    System.out.print(eName + eDescription+ eDate +eLocation+ eTickets+ ePrice);
                    Event e = new Event (eName, eDescription, eDate,
                              eLocation, eTickets, ePrice);
                    Event.insert(e);
                    oEvents.add(e);
                    showEvents();
                }
                    
                
            }
            
            bImport.setVisible(false);
            tfImport.setVisible(false);
            lImport.setVisible(false);
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FXMLAdminEventsController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(FXMLAdminEventsController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FXMLAdminEventsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        successfullyInserted.setVisible(false);
        notInserted.setVisible(false);
        bImport.setVisible(false);
        tfImport.setVisible(false);
        lImport.setVisible(false);
        showEvents();
    }

}
