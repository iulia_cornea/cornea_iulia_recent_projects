/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Event.Event;
import Event.EventGateway;
import Sale.Sale;
import User.User;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author iulia
 */
public class Test extends Application{
    
    public static User currentUser;
    
     @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("FXMLLogin.fxml"));
        
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.show();
    }
   
    
    public static void main(String[] args) {
        /*
        UserView.print(User.getAll());
        System.out.println();
        UserView.print(User.find("admin1@domain.com"));
        User.changePassword(User.find("admin1@domain.com"), "LLAAL");
        UserView.print(User.find("admin1@domain.com"));
        User.insert(new User (0, "nada.com", "ceva pass"));
        System.out.println(User.insert(new User (0, "cutotulaltceva@altceva.com", "ceva pass")));
        UserView.print(User.getAll());
        User.delete(new User (0, "nada.com", "ceva pass"));
        
        System.out.print("change pass for inexisten user" + User.changePassword(new User (0, "altceva@altceva.com", "ceva pass"), "CHANGED PASS"));
        UserView.print(User.getAll());
        System.out.println("**************************************************************");
        System.out.println(User.login("mama", "mia"));
        System.out.println(User.login("admin1@domain.com", "mia"));
        System.out.println(User.login("admin1@domain.com", "LLAAL"));
        
                */
        
        /*
        try {
            ResultSet rs = EventGateway.findEvent(1);
            rs.next();
            System.out.println(rs.getString("event_name"));
            rs = EventGateway.findEvents("UNTOLD FESTIVAL");
            rs.next();
            System.out.println(rs.getString("event_description"));
            
            EventGateway.insertEvent("Festivalul luminii", "description", new java.sql.Date(new java.util.Date().getTime()), "some location", 70, 120.3f);
            rs = EventGateway.findEvent(3);
            rs.next();
            System.out.println(rs.getString("event_name"));
            EventGateway.deleteEvent(8);
           
            rs = EventGateway.getAllEvents();
            while(rs.next())
            {
                System.out.println("*****" + rs.getInt("event_id") +"\t" +
                                rs.getString("event_name") + "\t" +
                                rs.getString("event_date"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        UserView.print(User.getAll());
        UserView.print(User.find("admin1@email.com"));
        if(null==User.find("admin10@domain.com"))
            System.out.println("Nup");
        User.changePassword(User.find("admin1@email.com"), "admin1pass");
        UserView.print(User.find("admin1@email.com"));
        System.out.println("*************************SALE*********************");
        System.out.println("Toate vanzarile");
        SaleView.print(Sale.getAll());        
        System.out.println("Vanzarile lui user1@email.com");
        SaleView.print(Sale.findByUserEmail("user1@email.com"));
        System.out.println("Vanzarile din 2015-04-01");
        SaleView.print(Sale.findByDate("2015-04-01"));
        System.out.println("Vanzarile la eventul cu id-ul 2");
        SaleView.print(Sale.findByEventId(2));
        System.out.println("Vanzarea cu id 2");
        SaleView.print(Sale.findById(2));
        System.out.println("Vanzarile lui user1@... la eventul cu id-ul 1");
        SaleView.print(Sale.findByUserEmailAndEventId("user1@email.com", 1));
        /*
        System.out.println("Inserare vanzare in 2016");
        if (Sale.insert(new Sale("user2@email.com",1,3,"2016-09-12")) == false){
                System.out.println("inserted");
        }
        else{
            System.out.println("not inserted");
        }
                */
        /*
        System.out.println("Toate vanzarile");        
        SaleView.print(Sale.getAll());
   //     SaleGateway.findSalesByDate(new java.sql.Date(new java.util.Date()));
     //   SaleGateway.findSalesByEventId();
       // SaleGateway.insertSale("admin1@domain.com", eventId, ticketsBought, null)
        
        
        ResultSet rs;
        
        try {
            rs = EventGateway.getAllEvents();
            while(rs.next())
            {
                System.out.println("*****" + rs.getInt("event_id") +"\t" +
                        rs.getString("event_name") + "\t" +
                        rs.getString("event_date") + "\t" +
                        rs.getInt("event_no_of_tickets"));
            }
            System.out.println("Delete 22");
            //Event.delete(Event.find(22));
            Event selectedEvent = Event.find(15);
            
            
        } catch (SQLException ex) {
            Logger.getLogger(Test.class.getName()).log(Level.SEVERE, null, ex);
        }
        */
        
        
        /*
        
        JSONObject user = new JSONObject();
        user.put("date", "2015...");
        user.put("email", "email..");
        user.put("bought", "12");
        
        JSONArray usersArray = new JSONArray();
        usersArray.put(user);
        
        
        JSONObject report = new JSONObject();
        report.put("date", "report date");
        report.put("users", usersArray);
        report.put("total", "1");
        
        JSONObject obj = new JSONObject();
	
 
	obj.put("report", report);
	try {
 
		FileWriter file = new FileWriter("test.json");
                StringWriter out = new StringWriter();
                obj.write(out);
                
		file.write(out.toString());
		file.flush();
		file.close();
 
	} catch (IOException e) {
		e.printStackTrace();
	}
 
	System.out.print(obj);
        */
        
         try {
            File file = new File("import_event.xml");
            DocumentBuilder dBuilder = DocumentBuilderFactory.newInstance()
                    .newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            if(!doc.getDocumentElement().getNodeName().equals("events")){
                //lImport.setText("File is not right format");
                return;
            }
            if(!doc.hasChildNodes()){
                //lImport.setText("There are no events to import");
                //lImport.setTextFill(Color.BLACK);
                return;
            }
            System.out.println(doc.getNodeName());
            System.out.println(doc.getChildNodes().item(0).getNodeName());
            
            /*
            //<events>
            NodeList eventsList = doc.getChildNodes();
            String eName = "", eDescription = "", eDate = "", eLocation = "";
            int eTickets = 0;
            float ePrice = 0.0f;
            //must go through each event
            for(int i = 0; i< eventsList.getLength(); i++){
                org.w3c.dom.Node eventNode =  eventsList.item(i);
                if(eventNode.hasChildNodes()){
                    NodeList eventDetails = eventNode.getChildNodes();
                    //must go through each deatail of the event
                    for(int j=0;j<eventDetails.getLength();j++){
                        org.w3c.dom.Node tempNode = eventDetails.item(j);
                        if(tempNode.getNodeName().equals("name")){
                            eName = tempNode.getTextContent();
                        }
                        if(tempNode.getNodeName().equals("description")){
                            eDescription = tempNode.getTextContent();
                        }
                        if(tempNode.getNodeName().equals("date")){
                            eDate = tempNode.getTextContent();
                        }
                        if(tempNode.getNodeName().equals("location")){
                            eLocation= tempNode.getTextContent();
                        }
                        if(tempNode.getNodeName().equals("tickets")){
                            eTickets = Integer.parseInt(tempNode.getTextContent());
                        }
                        if(tempNode.getNodeName().equals("price")){
                            ePrice = Float.parseFloat(tempNode.getTextContent());
                        } 
                        System.out.println(tempNode.getNodeName());
                    }
                    
                    System.out.print(eName + eDescription+ eDate +eLocation+ eTickets+ ePrice);
                    //Event.insert(new Event (eName, eDescription, eDate,
                    //            eLocation, eTickets, ePrice));
                }
                    
                
            }
            
            //bImport.setVisible(false);
            //tfImport.setVisible(false);
            //lImport.setVisible(false);
                    */
        } catch (ParserConfigurationException ex) {
            Logger.getLogger(FXMLAdminEventsController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SAXException ex) {
            Logger.getLogger(FXMLAdminEventsController.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(FXMLAdminEventsController.class.getName()).log(Level.SEVERE, null, ex);
        }
        launch(args);
        
    }
}
