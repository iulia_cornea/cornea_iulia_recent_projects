/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Event.Event;
import Reservation.Reservation;
import Sale.Sale;
import java.io.IOException;
import java.net.URL;
import java.sql.Date;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author iulia
 */
public class FXMLUserReservationsController implements Initializable {

    @FXML
    private TableView tReservations;
    @FXML
    private TableColumn cTicketsReserved;
    @FXML
    private TableColumn cReservationDate;

    void showReservations() {
        ArrayList<Reservation> reservations = Reservation.findByUserEmail(Test.currentUser.getEmail());
        ObservableList<Reservation> oReservations = FXCollections.observableArrayList(reservations);
        cTicketsReserved.setCellValueFactory(new PropertyValueFactory<Reservation, Integer>("ticketsReserved"));
        cReservationDate.setCellValueFactory(new PropertyValueFactory<Reservation, String>("date"));

        tReservations.setItems(oReservations);
    }

    @FXML
    private Label lDetails;
    @FXML
    private Label lStatus;

    @FXML
    private void goToUserPage(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLUserPage.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(home_page_scene);
        app_stage.show();
    }

    @FXML
    private void showDetails() {
        Reservation selectedReservation = (Reservation) tReservations.getSelectionModel().getSelectedItem();
        Event selectedEvent = Event.find(selectedReservation.getEventId());
        lDetails.setText("Reservation to " + selectedEvent.getName()
                + " from " + selectedEvent.getDate());
        lDetails.setVisible(true);
    }

    @FXML
    private void finalizeOrder() {
        lDetails.setVisible(false);
        lStatus.setVisible(false);
        Reservation selectedReservation = (Reservation) tReservations.getSelectionModel().getSelectedItem();
        if (selectedReservation == null) {
            lDetails.setText("Must select a reservation");
            lDetails.setVisible(true);
            return;
        }
        Event selectedEvent = Event.find(selectedReservation.getEventId());

        ArrayList<Sale> sales = Sale.findByUserEmailAndEventId(Test.currentUser.getEmail(), selectedEvent.getId());
        int ticketsBought = 0;
        for (Sale s : sales) {
            ticketsBought += s.getTicketsBought();
        }

        //delete reservation
        if (ticketsBought == 5) {
            lStatus.setText("You have already bought 5 tickets to this event. Your reservation has been deleted");
            lStatus.setVisible(true);
            Reservation.delete(selectedReservation);
            Event.update(selectedEvent, selectedEvent.getName(),
                    selectedEvent.getDescription(), selectedEvent.getDate(),
                    selectedEvent.getLocation(), selectedEvent.getNoOfTickets(),
                    selectedEvent.getReservedTickets() - selectedReservation.getTicketsReserved(), 
                    selectedEvent.getAvailableTickets() + selectedReservation.getTicketsReserved(),
                    selectedEvent.getTicketPrice());
            showReservations();
            return;
        }
        //update reservation
        if (ticketsBought + selectedReservation.getTicketsReserved() > 5) {
            lStatus.setText("You have already bought " + ticketsBought
                    + " tickets to this event. Your reservation has been updated");
            lStatus.setVisible(true);
            Reservation.setTickets(selectedReservation, 5 - ticketsBought);
            Event.update(selectedEvent, selectedEvent.getName(),
                    selectedEvent.getDescription(), selectedEvent.getDate(),
                    selectedEvent.getLocation(), selectedEvent.getNoOfTickets(),
                    selectedEvent.getReservedTickets() - (selectedReservation.getTicketsReserved() - (5 - ticketsBought)), 
                    selectedEvent.getAvailableTickets() + selectedReservation.getTicketsReserved() - (5 - ticketsBought),
                    selectedEvent.getTicketPrice());
            showReservations();
            return;
        }
        
        // can finalize reservation
         if (ticketsBought + selectedReservation.getTicketsReserved() <= 5){
             int total = ticketsBought + selectedReservation.getTicketsReserved();
            lStatus.setText("You have bought " + total
                    + " tickets to this event. Your reservation has now been deleted");
            lStatus.setVisible(true);
            Reservation.delete(selectedReservation);
            Event.update(selectedEvent, selectedEvent.getName(),
                    selectedEvent.getDescription(), selectedEvent.getDate(),
                    selectedEvent.getLocation(), selectedEvent.getNoOfTickets(),
                    selectedEvent.getReservedTickets() - selectedReservation.getTicketsReserved(), 
                    selectedEvent.getAvailableTickets(),
                    selectedEvent.getTicketPrice());
            Sale.insert(new Sale(Test.currentUser.getEmail(),
                                    selectedReservation.getEventId(),
                                    selectedReservation.getTicketsReserved(),
                                    selectedReservation.getDate()));
            showReservations();
            return;
            
         }
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        lDetails.setVisible(false);
        lStatus.setVisible(false);
        showReservations();
    }

}
