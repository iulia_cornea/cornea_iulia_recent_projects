/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import User.User;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author iulia
 */
public class FXMLUserSignUpController implements Initializable {
    
    @FXML
    private TextField email;
    @FXML
    private PasswordField pass1;
    @FXML
    private PasswordField pass2;
    @FXML
    private Label emailNotValid;
    @FXML
    private Label accountExists;
    @FXML
    private Label passNotMatch;
    @FXML
    private Button signUp;
    
    
    @FXML
    private void signUp(ActionEvent event) throws IOException{
        accountExists.setVisible(false);
        passNotMatch.setVisible(false);  
        emailNotValid.setVisible(false);
        
        Pattern p = Pattern.compile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$");
        Matcher m = p.matcher(email.getText());
        if(!m.matches()){
            emailNotValid.setVisible(true);
            return;
        }
        if(User.find(email.getText()) != null){
            accountExists.setVisible(true);  
            return;
        }
        if(!pass1.getText().equals(pass2.getText())){ 
            passNotMatch.setVisible(true);
            return;
        }
        Test.currentUser = new User(1,email.getText(),pass1.getText(), "", 0);
        User.insert(Test.currentUser); 
        Parent successfully_sign_up_parent = FXMLLoader.load(getClass().getResource("FXMLSuccessfullySignUp.fxml"));
        Scene successfully_sign_up_scene = new Scene(successfully_sign_up_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(successfully_sign_up_scene);
        app_stage.show();
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
}
