/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Event.Event;
import Sale.Sale;
import User.User;
import java.io.IOException;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.TextArea;
import javafx.stage.Stage;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.json.JSONArray;
import org.json.JSONObject;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 * FXML Controller class
 *
 * @author iulia
 */
public class FXMLAdminReportsController implements Initializable {

    @FXML
    private CheckBox cbTXT;
    @FXML
    private CheckBox cbXML;
    @FXML
    private CheckBox cbJSON;

    @FXML
    private TextArea textArea;

    private void fillTextArea(String file) {
        try {
            List<String> report;
            report = Files.readAllLines(Paths.get(file));
            String text = "";
            for (String t : report) {
                text = text + t;
            }
            textArea.setText(text);
        } catch (IOException ex) {
            Logger.getLogger(FXMLAdminReportsController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void generateSoldTicketsReport() throws IOException {
        List<String> report;
        if (cbTXT.isSelected()) {
            txtSoldTicketsReport();
        }
        if (cbXML.isSelected()) {
            xmlSoldTicketsReport(Event.getAll());
            fillTextArea("xml_event_report.xml");
        }
        if (cbJSON.isSelected()) {
            jSonSoldTicketsReport(Event.getAll());
            fillTextArea("json_event_report.json");
        }
    }

    @FXML
    private void generateRegisteredUsersReport() throws IOException, ClassNotFoundException {
        List<String> report;
        if (cbTXT.isSelected()) {
            txtRegisteredUserReport();
        }
        if (cbXML.isSelected()) {
            xmlRegisteredUsersReport(User.getAll());
            fillTextArea("xml_user_report.xml");
        }
        if (cbJSON.isSelected()) {
            jSonRegisteredUsersReport(User.getAll());
            fillTextArea("json_user_report.json");
        }

    }

    private void txtSoldTicketsReport() {
        ArrayList<Event> events = Event.getAll();
        ArrayList<Sale> salesForEvent;
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(Calendar.getInstance().getTime());

        String report = "Report from: " + date + "\n";
        int ticketsSold;
        for (Event e : events) {
            ticketsSold = e.getNoOfTickets() - e.getReservedTickets() - e.getAvailableTickets();
            report = report + "Event: " + e.getName() + " Starting no. of tickets: "
                    + e.getNoOfTickets() + " Tickets sold: " + ticketsSold + '\n';
        }
        textArea.setText(report);
    }

    private void txtRegisteredUserReport() {
        ArrayList<User> users = User.getAll();
        ArrayList<Sale> userSales;
        int noOfUsers;
        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(Calendar.getInstance().getTime());

        noOfUsers = 0;
        String report = "Report from: " + date + "\n";
        for (User u : users) {
            if (u.getType() == 1) {
                noOfUsers++;
                report += "User email: " + u.getEmail()
                        + " Registered at: " + u.getRegistrationDate()
                        + " bought a toatal of: " + u.getTicketsBought()
                        + " tickets" + '\n';
            }
        }
        report += "The total number of registered users: " + noOfUsers + '\n';
        textArea.setText(report);
    }

    private void xmlSoldTicketsReport(ArrayList<Event> eventsList) {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element docRoot = doc.createElement("events");
            doc.appendChild(docRoot);

            for (Event event : eventsList) {
                // root elements

                Element rootElement = doc.createElement("event");
                docRoot.appendChild(rootElement);

                // staff elements
                Element name = doc.createElement("name");
                name.appendChild(doc.createTextNode(event.getName()));
                rootElement.appendChild(name);

                // staff elements
                Element description = doc.createElement("description");
                description.appendChild(doc.createTextNode(event.getDescription()));
                rootElement.appendChild(description);

                Element date = doc.createElement("date");
                date.appendChild(doc.createTextNode(event.getDate()));
                rootElement.appendChild(date);

                Element location = doc.createElement("location");
                location.appendChild(doc.createTextNode(event.getLocation()));
                rootElement.appendChild(location);

                Element tickets = doc.createElement("tickets");
                tickets.appendChild(doc.createTextNode("" + event.getNoOfTickets()));
                rootElement.appendChild(tickets);

                Element price = doc.createElement("price");
                price.appendChild(doc.createTextNode("" + event.getTicketPrice()));
                rootElement.appendChild(price);
            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult("xml_event_report.xml");

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);
            transformer.transform(source, result);

            System.out.println("File saved!");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    private void xmlRegisteredUsersReport(ArrayList<User> usersList) {
        try {

            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element docRoot = doc.createElement("users");
            doc.appendChild(docRoot);

            for (User user : usersList) {
                // root elements

                Element rootElement = doc.createElement("user");
                docRoot.appendChild(rootElement);

                Element date = doc.createElement("date");
                date.appendChild(doc.createTextNode(user.getRegistrationDate()));
                rootElement.appendChild(date);

                Element email = doc.createElement("email");
                email.appendChild(doc.createTextNode(user.getEmail()));
                rootElement.appendChild(email);

                Element tickets = doc.createElement("bought");
                tickets.appendChild(doc.createTextNode("" + user.getTicketsBought()));
                rootElement.appendChild(tickets);

            }

            // write the content into xml file
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult("xml_user_report.xml");

            // Output to console for testing
            // StreamResult result = new StreamResult(System.out);
            transformer.transform(source, result);

            System.out.println("File saved!");

        } catch (ParserConfigurationException pce) {
            pce.printStackTrace();
        } catch (TransformerException tfe) {
            tfe.printStackTrace();
        }
    }

    private void jSonSoldTicketsReport(ArrayList<Event> eventsList) {

        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(Calendar.getInstance().getTime());

        JSONObject obj = new JSONObject();
        JSONObject report = new JSONObject();
        JSONArray eventsArray = new JSONArray(); 
        
        obj.put("report", report);
        report.put("date", date);
        
        for(Event e : eventsList){
            JSONObject event = new JSONObject();
            event.put("id", e.getId());
            event.put("name", e.getName());
            event.put("tickets", e.getNoOfTickets());
            int bought = e.getNoOfTickets()-e.getAvailableTickets()-e.getReservedTickets();
            event.put("bought", bought);
            
            eventsArray.put(event);
        }
        
        report.put("events", eventsArray);
        report.put("total", eventsList.size());
        
        try {

            FileWriter file = new FileWriter("json_event_report.json");
            StringWriter out = new StringWriter();
            obj.write(out);

            file.write(out.toString());
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void jSonRegisteredUsersReport(ArrayList<User> usersList) {

        Format formatter = new SimpleDateFormat("yyyy-MM-dd");
        String date = formatter.format(Calendar.getInstance().getTime());

        JSONObject obj = new JSONObject();
        JSONObject report = new JSONObject();
        JSONArray usersArray = new JSONArray();

        obj.put("report", report);

        report.put("date", date);

        int total = 0;
        for (User u : usersList) {
            if (u.getType() != 0) {
                JSONObject user = new JSONObject();
                user.put("date", u.getRegistrationDate());
                user.put("email", u.getEmail());
                user.put("bought", u.getTicketsBought());

                usersArray.put(user);
                total++;
            }
        }

        report.put("users", usersArray);
        report.put("total", total);

        try {

            FileWriter file = new FileWriter("json_user_report.json");
            StringWriter out = new StringWriter();
            obj.write(out);

            file.write(out.toString());
            file.flush();
            file.close();

        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.print(obj);
    }

    @FXML
    private void goToAdminPage(ActionEvent event) throws IOException {
        Parent home_page_parent = FXMLLoader.load(getClass().getResource("FXMLAdminPage.fxml"));
        Scene home_page_scene = new Scene(home_page_parent);
        Stage app_stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        app_stage.hide();
        app_stage.setScene(home_page_scene);
        app_stage.show();
        return;
    }

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }

}
