/////////////////////////////////////////////////////////////////////////////////////
//																				   //								   //
//																				   //
//	Use W A S D keys to rotate the camera										   //	
//																				   //
/////////////////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "math.h"
#include "tga.h"
#include "glut.h"
#include "glm.h"

int screen_width=640;
int screen_height=480;

// movementAngle of rotation for the camera direction
float movementAngle=0.0;
// actual vector representing the camera's direction
float cameraDirectionX=0.0f,cameraDirectionZ=-1.0f;
// XZ position of the camera
float CameraEyeX=0.0f,CameraEyeZ=5.0f;

GLenum mod=GL_FILL;
int animatie = 0;
GLfloat animatieAngle = 0.0;
GLfloat fGlobalAngleX, fGlobalAngleY, fGlobalAngleZ; //global rotation angles
GLfloat lightFelinar = 0;
GLfloat lightAngle =0.1;
GLfloat doorAngle = 0.0;
GLMmodel *cub, *bradMic, *bradMare, *casa, *usa, *felinar, *licurici0, *licurici1, *licurici2;
GLuint cubTexture, bradMicTexture, bradMareTexture, casaTexture, usaTexture, felinarTexture, licurici0Texture, licurici1Texture, licurici2Texture;

//equations of the planes on which the shadow is cast
GLfloat fvFloorPlaneEq[4];
//projected shadows matrices
GLfloat fvFloorShadowMat[16];

GLfloat fogControl =0;
GLfloat fogIntensity = 0.5;
GLfloat fogColor[3] = {fogIntensity, fogIntensity, fogIntensity};

GLfloat mylightIntensity = 0.1;

GLfloat mylight_ambient[] = {mylightIntensity, mylightIntensity, mylightIntensity, 0.1};
GLfloat mylight_diffuse[] = {1.0, 1.0, 1.0, 1.0};
GLfloat mylight_specular[] = {1.0, 1.0, 1.0, 1.0};
GLfloat mylight_position[] = {0, 5, 0, 1};

GLfloat mylight1_ambient[] = {0.9, 0.9, 0, 0.1};
GLfloat mylight1_diffuse[] = {1.0, 1.0, 1.0, 1.0};
GLfloat mylight1_specular[] = {1.0, 1.0, 0, 1.0};
GLfloat mylight1_position[] = {0, 0, 0, 1};

void PlaneEq(GLfloat plane[4], GLfloat p0[4], GLfloat p1[4], GLfloat p2[4]);
void ComputePlaneEquations();
void ComputeShadowMatrix(GLfloat shadowMat[16], GLfloat plane[4], GLfloat lightPos[4]);
void ComputeShadowMatrices();

void PlaneEq(GLfloat plane[4], GLfloat p0[4], GLfloat p1[4], GLfloat p2[4])
{
	GLfloat vec0[3], vec1[3];

	vec0[0] = p1[0] - p0[0];
	vec0[1] = p1[1] - p0[1];
	vec0[2] = p1[2] - p0[2];

	vec1[0] = p2[0] - p0[0];
	vec1[1] = p2[1] - p0[1];
	vec1[2] = p2[2] - p0[2];

	plane[0] = vec0[1] * vec1[2] - vec0[2] * vec1[1];
	plane[1] = -(vec0[0] * vec1[2] - vec0[2] * vec1[0]);
	plane[2] = vec0[0] * vec1[1] - vec0[1] * vec1[0];

	plane[3] = -(plane[0] * p0[0] + plane[1] * p0[1] + plane[2] * p0[2]);
}
void ComputePlaneEquations()
{
	//floor points
	GLfloat fvFloorP0[4] = {2, -2, 2, 1.0}; 
	GLfloat fvFloorP1[4] = {2, -2, -2, 1.0};
	GLfloat fvFloorP2[4] = {-2, -2, -2, 1.0};

	PlaneEq(fvFloorPlaneEq, fvFloorP0, fvFloorP1, fvFloorP2);
}
void ComputeShadowMatrices()
{
	GLfloat lightpos[4] = {2,0,2,1.0};
	ComputeShadowMatrix(fvFloorShadowMat, fvFloorPlaneEq, lightpos);
}
void ComputeShadowMatrix(GLfloat shadowMat[16], GLfloat plane[4], GLfloat lightPos[4])
{
	GLfloat dotProduct;

	dotProduct = plane[0] * lightPos[0] +
				 plane[1] * lightPos[1] +
				 plane[2] * lightPos[2] +
				 plane[3] * lightPos[3];

	shadowMat[0] = dotProduct - lightPos[0] * plane[0];
	shadowMat[1] = 0.0f - lightPos[1] * plane[0];
	shadowMat[2] = 0.0f - lightPos[2] * plane[0];
	shadowMat[3] = 0.0f - lightPos[3] * plane[0];

	shadowMat[4] = 0.0f - lightPos[0] * plane[1];
	shadowMat[5] = dotProduct - lightPos[1] * plane[1];
	shadowMat[6] = 0.0f - lightPos[2] * plane[1];
	shadowMat[7] = 0.0f - lightPos[3] * plane[1];

	shadowMat[8] = 0.0f - lightPos[0] * plane[2];		
	shadowMat[9] = 0.0f - lightPos[1] * plane[2];
	shadowMat[10] = dotProduct - lightPos[2] * plane[2];
	shadowMat[11] = 0.0f - lightPos[3] * plane[2];

	shadowMat[12] = 0.0f - lightPos[0] * plane[3];
	shadowMat[13] = 0.0f - lightPos[1] * plane[3];		
	shadowMat[14] = 0.0f - lightPos[2] * plane[3];		
	shadowMat[15] = dotProduct - lightPos[3] * plane[3];
}

void drawBradMare(){
	int i,j, pozX = -10, pozZ = -10;
	glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,bradMareTexture);
		glScaled(1.5,2,1.5);
		for(i=0; i<12; i++){
			for(j=0; j<12; j++){
				if(((i<4) || (i>7)) || ((j<4) || (j>6))) {
				glPushMatrix();
					glTranslatef(pozX,0,pozZ);
					glmDraw(bradMare,GLM_SMOOTH|GLM_TEXTURE);
				glPopMatrix();
				}
			pozX = pozX + 2;
			}
			pozZ = pozZ + 2;
			if(i%2 == 0)
				pozX = -10;
			else 
				pozX = -11;
		}
		glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	

}

void drawCasa(){
		glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,casaTexture);
		glTranslatef(0,-1.1,-1);
		glScalef(2,2,2);
		glmDraw(casa,GLM_SMOOTH|GLM_TEXTURE);
		glDisable(GL_TEXTURE_2D);
	glPopMatrix();
}

void drawBradMic(){
	glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,bradMicTexture);
		glTranslatef(-3,-1.1,3);
		glmDraw(bradMic,GLM_SMOOTH|GLM_TEXTURE);
		glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,bradMicTexture);
		glTranslatef(3,-1.1,1);
		glmDraw(bradMic,GLM_SMOOTH|GLM_TEXTURE);
		glDisable(GL_TEXTURE_2D);
	glPopMatrix();

}

void processSpecialKeys(int key, int xx, int yy) {

	float fraction = 0.1f;

	switch (key) {
		case GLUT_KEY_LEFT :
			movementAngle -= 0.01f;
			cameraDirectionX = sin(movementAngle);
			cameraDirectionZ = -cos(movementAngle);
			break;
		case GLUT_KEY_RIGHT :
			movementAngle += 0.01f;
			cameraDirectionX = sin(movementAngle);
			cameraDirectionZ = -cos(movementAngle);
			break;
		case GLUT_KEY_UP :
			CameraEyeX += cameraDirectionX * fraction;
			CameraEyeZ += cameraDirectionZ * fraction;
			break;
		case GLUT_KEY_DOWN :
			CameraEyeX -= cameraDirectionX * fraction;
			CameraEyeZ -= cameraDirectionZ * fraction;
			break;
	}
}

void EnableFog()
{
glClearColor(fogColor[0], fogColor[1], fogColor[2], 1.0);
glFogfv(GL_FOG_COLOR, fogColor);
glFogi(GL_FOG_MODE, GL_EXP);
glFogf(GL_FOG_DENSITY, 0.1f);
glEnable(GL_FOG);
}

void initOpenGL()
{
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
	glShadeModel(GL_SMOOTH);
	glViewport(0, 0, screen_width, screen_height);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0f, (GLfloat)screen_width/(GLfloat)screen_height, 1.0f, 1000.0f);
	glEnable(GL_DEPTH_TEST);
	glPolygonMode(GL_FRONT_AND_BACK, mod);
	glMatrixMode(GL_MODELVIEW);	

	glEnable(GL_LIGHTING);
	glEnable(GL_LIGHT0);
	glEnable(GL_NORMALIZE);

	///////////////////////////////////////////////////////// LIGHT0
	glLightfv(GL_LIGHT0, GL_AMBIENT, mylight_ambient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, mylight_diffuse);
	glLightfv(GL_LIGHT0, GL_SPECULAR, mylight_specular);

	ComputePlaneEquations();
	ComputeShadowMatrices();

	///////////////////////////////////////////////////////// licurici0
	licurici0 = glmReadOBJ("objects/licurici.obj");
	glmUnitize(licurici0);
	glmFacetNormals(licurici0);
	glmVertexNormals(licurici0,90.0);
	
	glGenTextures(1,&licurici0Texture);
	loadTGA("objects/licurici.tga",licurici0Texture);

	///////////////////////////////////////////////////////// licurici1
	licurici1 = glmReadOBJ("objects/licurici.obj");
	glmUnitize(licurici1);
	glmFacetNormals(licurici1);
	glmVertexNormals(licurici1,90.0);
	
	glGenTextures(1,&licurici1Texture);
	loadTGA("objects/licurici.tga",licurici1Texture);

	///////////////////////////////////////////////////////// licurici2
	licurici2 = glmReadOBJ("objects/licurici.obj");
	glmUnitize(licurici2);
	glmFacetNormals(licurici2);
	glmVertexNormals(licurici2,90.0);
	
	glGenTextures(1,&licurici2Texture);
	loadTGA("objects/licurici.tga",licurici2Texture);

	///////////////////////////////////////////////////////// cub
	cub = glmReadOBJ("objects/cub.obj");
	glmUnitize(cub);
	glmFacetNormals(cub);
	glmVertexNormals(cub,90.0);
	
	glGenTextures(1,&cubTexture);
	loadTGA("objects/cub.tga",cubTexture);

	///////////////////////////////////////////////////////// bradMic
	bradMic = glmReadOBJ("objects/bradMic.obj");
	glmUnitize(bradMic);
	glmFacetNormals(bradMic);
	glmVertexNormals(bradMic,90.0);

	glGenTextures(1,&bradMicTexture);
	loadTGA("objects/bradMic.tga",bradMicTexture);

	///////////////////////////////////////////////////////// bradMare
	bradMare = glmReadOBJ("objects/brradMare.obj");
	glmUnitize(bradMare);
	glmFacetNormals(bradMare);
	glmVertexNormals(bradMare,90.0);

	glGenTextures(1,&bradMareTexture);
	loadTGA("objects/brradMare.tga",bradMareTexture);

	///////////////////////////////////////////////////////// casa
	casa = glmReadOBJ("objects/casa.obj");
	glmUnitize(casa);
	glmFacetNormals(casa);
	glmVertexNormals(casa,90.0);

	glGenTextures(1,&casaTexture);
	loadTGA("objects/casa.tga",casaTexture);

	///////////////////////////////////////////////////////// usa
	usa = glmReadOBJ("objects/door.obj");
	glmUnitize(usa);
	glmFacetNormals(usa);
	glmVertexNormals(usa,90.0);

	glGenTextures(1,&usaTexture);
	loadTGA("objects/door.tga",usaTexture);

	///////////////////////////////////////////////////////// felinar
	felinar = glmReadOBJ("objects/lompa.obj");
	glmUnitize(felinar);
	glmFacetNormals(felinar);
	glmVertexNormals(felinar,90.0);

	glGenTextures(1,&felinarTexture);
	loadTGA("objects/lompa.tga",felinarTexture);
}

void renderScene(void)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glLoadIdentity();

	gluLookAt(	CameraEyeX, -1, CameraEyeZ,
			CameraEyeX+cameraDirectionX, -1,  CameraEyeZ+cameraDirectionZ,
			0.0f, 1.0f,  0.0f);
	//set global rotation on the X,Y and Z axes
	glRotatef(fGlobalAngleX, 1.0, 0.0, 0.0);
	glRotatef(fGlobalAngleY, 0.0, 1.0, 0.0);
	glRotatef(fGlobalAngleZ, 0.0, 0.0, 1.0);

	if(animatie){
		animatieAngle += 0.5;
		//glTranslatef(1,1,1);
		glRotatef(animatieAngle,0,1,0);
		//glTranslatef(-1,-1,-1);
	}

	glLightfv(GL_LIGHT0, GL_POSITION, mylight_position);
	if(fogControl)
		EnableFog();

	if(lightFelinar){
	//enable stencil testing
	glEnable(GL_STENCIL_TEST);
	//set the stencil function to keep all data except when depth test fails
    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

	/////////////////////////////////////////////////////////////////////////////////////////////////////////// FLOOR

	//set stencil function to allways pass
	//this way the stencil buffer will be 1 in the area where the floor is drawn
	glStencilFunc(GL_ALWAYS, 1, 0);    
	//clear the stencil buffer
	glClear(GL_STENCIL_BUFFER_BIT);

	glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,cubTexture);
	glScalef(30,10,30);
	glTranslatef(0,0.8,0);
	glmDraw(cub,GLM_SMOOTH|GLM_TEXTURE);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	//set the stencil function to pass only when the stencil buffer is 1
	//this way the shadow will appear only on the surface previously drawn
	glStencilFunc(GL_EQUAL, 1, 1);
	//disable depth testing for the shadow
	glDisable(GL_DEPTH_TEST);
	//disable lighting, in order to have a black shadow
	glDisable(GL_LIGHTING);

	//floor shadow	
	glColor3f(0.0f, 0.0f, 0.0f);//set the shadow color
	glPushMatrix();
		glMultMatrixf(fvFloorShadowMat);
		drawBradMare();
		drawBradMic();
		drawCasa();
		glPushMatrix();
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,felinarTexture);
			glScalef(1,1,1);
			glTranslatef(-1,-1.5,1);
			glmDraw(felinar,GLM_SMOOTH|GLM_TEXTURE);
			glDisable(GL_TEXTURE_2D);
		glPopMatrix();
	glPopMatrix();
	//enable depth testing
	glEnable(GL_DEPTH_TEST);
	//enable lighting
	glEnable(GL_LIGHTING);
	}

	if(!lightFelinar){
		glPushMatrix();
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D,cubTexture);
	glScalef(30,10,30);
	glTranslatef(0,0.8,0);
	glmDraw(cub,GLM_SMOOTH|GLM_TEXTURE);
	glDisable(GL_TEXTURE_2D);
	glPopMatrix();
	}

	drawBradMare();
	drawBradMic();
	drawCasa();
	/////////////////////////////////////////////////////////////////////////////////////// USA
	glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,usaTexture);
		
		glTranslatef(-0.4,-1.75,-0.4);
		glTranslatef(0.2,0,0);
		glRotatef(doorAngle,0,1,0);
		glTranslatef(-0.2,0,0);
		glScalef(0.7,0.7,0.5);
		glmDraw(usa,GLM_SMOOTH|GLM_TEXTURE);
		glDisable(GL_TEXTURE_2D);
	glPopMatrix();

	glPushMatrix();
			glEnable(GL_TEXTURE_2D);
			glBindTexture(GL_TEXTURE_2D,felinarTexture);
			glScalef(1,1,1);
			glTranslatef(-1,-1.5,1);
			glmDraw(felinar,GLM_SMOOTH|GLM_TEXTURE);
			glDisable(GL_TEXTURE_2D);
		glPopMatrix();

	glPushMatrix();
		glEnable(GL_TEXTURE_2D);
		glBindTexture(GL_TEXTURE_2D,felinarTexture);
		glScalef(2,2,2);
		glTranslatef(1,-0.5,1);
		glmDraw(felinar,GLM_SMOOTH|GLM_TEXTURE);
		glDisable(GL_TEXTURE_2D);

		glTranslatef(0,0.34,0);
		glutSolidSphere(0.08,20,20);
		///////////////////////////////////////////////////////// LIGHT1
		if(lightFelinar == 1){
			glEnable(GL_LIGHT1);
			glLightfv(GL_LIGHT1, GL_AMBIENT, mylight1_ambient);
			glLightfv(GL_LIGHT1, GL_DIFFUSE, mylight1_diffuse);
			glLightfv(GL_LIGHT1, GL_SPECULAR, mylight1_specular);
			glLightfv(GL_LIGHT1, GL_POSITION, mylight1_position);
		}
		else
			glDisable(GL_LIGHT1);
		glPushMatrix();
			lightAngle += 2;
			//pune licurici0
			glPushMatrix();
				glRotatef(lightAngle,0,1,0);
				glTranslatef(0.3,0,0);				
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D,licurici0Texture);
				glScalef(0.01,0.01,0.01);
				glmDraw(licurici0,GLM_SMOOTH|GLM_TEXTURE);
				glDisable(GL_TEXTURE_2D);
			glPopMatrix();
			//pune licurici1
			glPushMatrix();
				glRotatef(lightAngle,1,1,0);
				glTranslatef(0.3,0,0);				
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D,licurici1Texture);
				glScalef(0.01,0.01,0.01);
				glmDraw(licurici1,GLM_SMOOTH|GLM_TEXTURE);
				glDisable(GL_TEXTURE_2D);
			glPopMatrix();
			//pune licurici2
			glPushMatrix();
				glRotatef(-lightAngle,0,1,1);
				glTranslatef(0.2,0,0);				
				glEnable(GL_TEXTURE_2D);
				glBindTexture(GL_TEXTURE_2D,licurici2Texture);
				glScalef(0.01,0.01,0.01);
				glmDraw(licurici2,GLM_SMOOTH|GLM_TEXTURE);
				glDisable(GL_TEXTURE_2D);
			glPopMatrix();
		glPopMatrix();
	glPopMatrix();

	if(fogControl)
		glDisable(GL_FOG);

	glutSwapBuffers(); //swap the buffers used in the double-buffering technique
}

void changeSize(int w, int h)
{
	screen_width=w;
	screen_height=h;

	if(h == 0)
		h = 1;

	float ratio = 1.0*w/h;

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glViewport(0, 0, w, h);
	gluPerspective(45.0f, ratio, 1.0f, 1000.0f);
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	gluLookAt(0.0f, 0.0f, 50.0f, 0.0f, 0.0f, -1.0f, 0.0f, 1.0f, 0.0f);
}

void processNormalKeys(unsigned char key, int x, int y)
{
	
	switch(key)
	{
		case 't':
			//process
			glutPostRedisplay();
			break;
		case 27: //esc
			exit(1);
			break;
		//control the global Y rotation angle using 'a' and 'd'
		case 'a': 
			fGlobalAngleY += 1;
			if (fGlobalAngleY >= 360) //clamp the rotation angle in the [0,360) interval
				fGlobalAngleY = (GLint)fGlobalAngleY % 360;
			break;
		case 'd':
			fGlobalAngleY -= 1;
			if (fGlobalAngleY <= -360) //clamp the rotation angle in the [0,360) interval
				fGlobalAngleY = (GLint)fGlobalAngleY % 360;			
			break;
		//control the global X rotation angle using 'w' and 's'
		case 'w':
			fGlobalAngleX += 1;
			if (fGlobalAngleX >= 360) //clamp the rotation angle in the [0,360) interval
				fGlobalAngleX = (GLint)fGlobalAngleX % 360;			
			break;
		case 's':
			fGlobalAngleX -= 1;
			if (fGlobalAngleX <= -360) //clamp the rotation angle in the [0,360) interval
				fGlobalAngleX = (GLint)fGlobalAngleX % 360;	
			break;
		//control the global Z rotation angle using 'q' and 'e'
		case 'q':
			fGlobalAngleZ += 1;
			if (fGlobalAngleZ >= 360) //clamp the rotation angle in the [0,360) interval
				fGlobalAngleZ = (GLint)fGlobalAngleZ % 360;	
			break;
		case 'e':
			fGlobalAngleZ -= 1;
			if (fGlobalAngleZ <= -360) //clamp the rotation angle in the [0,360) interval
				fGlobalAngleZ = (GLint)fGlobalAngleZ % 360;	
			break;	
		//////////////////////////////////////////////////////////////////////////////////////// light l
		case 'l':
			if(lightFelinar == 0)
				lightFelinar = 1.0;
			else
				lightFelinar = 0.0;
			break;
			//////////////////////////////////////////////////////////////////////////////////////// fog f
		case 'f':
			if(fogControl == 0)
				fogControl = 1.0;
			else
				fogControl = 0.0;
			break;
			//////////////////////////////////////////////////////////////////////////////////////// inside door u
		case 'u':
			if(doorAngle < 70)
				doorAngle ++;
			break;
			//////////////////////////////////////////////////////////////////////////////////////// outside door i
		case 'i':
			if(doorAngle > -80)
				doorAngle --;
			break;
			//////////////////////////////////////////////////////////////////////////////////////// mod vizualizare
		case 'm':
			if(mod == GL_FILL)
				mod = GL_LINE;
			else
				mod = GL_FILL;
			initOpenGL();

			break;
		case 'x':
			if(animatie == 1)
				animatie = 0;
			else
				animatie = 1;
			break;
		
	}
	

}

int main(int argc, char* argv[])
{
	//Initialize the GLUT library
	glutInit(&argc, argv);
	//Set the display mode
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	//Set the initial position and dimensions of the window
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(screen_width, screen_height);
	//creates the window
	glutCreateWindow("First OpenGL Application");
	//Specifies the function to call when the window needs to be redisplayed
	glutDisplayFunc(renderScene);
	//Sets the idle callback function
	glutIdleFunc(renderScene);
	//Sets the reshape callback function
	glutReshapeFunc(changeSize);
	//Keyboard callback function
	glutKeyboardFunc(processNormalKeys);
	glutSpecialFunc(processSpecialKeys);
	//Initialize some OpenGL parameters
	initOpenGL();
	//Starts the GLUT infinite loop
	glutMainLoop();
	return 0;
}

