# README #



### Image Processing - assignments from class ###

The project I wrote with a classmate is in Project. It is an attempt to a program which determines the proximity of the objects in the picture. Closer objects should appear darker after processing.

### Java App - assigments from class ###

Ticketing system app. School project. Implementation of the following assignments

**Assigment A1**

1. Create a desktop/web application that helps managing a ticketing system for events (like concerts or festivals). For this assignment, three user types are required.

* The administrator can add events (name, description, date, location, number of tickets, ticket price) and can generate reports of sold tickets and registered users.

* The visitor can only view the list of events (name, description, date, location, number of available tickets, ticket price).

* The normal user can register on the application with an email and password change the personal profile details and buy up to 5 tickets per event. Both the administrator and the normal user should log in using an email and a password.

2. Application Constraints

* The data will be stored in a relational database.

* Use the Layers architectural pattern to organize your application.

* Use a domain logic pattern (transaction script or domain model) / a data source hybrid pattern (table module, active record) and a data source pure pattern (table data gateway, row data gateway, data mapper) most suitable for the application.

3. Requirements
* Create the analysis and design document (see the template).
* Implement and test the application.
4. Deliverables
* Analysis and design document.
* Implementation source files.

**Assigment A2**

1. Your task is to enhance with additional functionalities the application built as Assignment A1 for managing a ticketing system for event.
* Both the normal user and the administrator can now reset a forgotten password by sending an email – integrate your application with SendGrid (https://sendgrid.com/home-one - the free plan allows you to send 400 emails/day – integration examples are provided)

* The administrator can now:

  1. Import events from an XML format similar to the one below


   
```
#!xml

 <events>

      <event>

        <name>We Are Electric Festival</name>

        <description>The Prodigy in concert.</description>

        <date>June 20, 2015</date>

        <location>Eindhoven, Netherlands</location>

        <tickets>1985</tickets>

        <price>10.90</price>

      </event>

    </events>
```


  
  2. Generate the reports of registered users (generation date, then for each user email, registration date, number of tickets bought + the total users) and sold tickets (generation date, then for each event number of total tickets and number of sold tickets) in the following formats (use Factory Method):

    ** JSON Format – example below **

      
```
#!json

{"report": {

        "date": "2015.04.21",

        "users": [{

            "date": "2015.03.01",

            "email": "mail@yahoo.com",

            "bought": 14

           }],

          "total": 1

        }

      }
```

  
    ** XML Format **

  * The normal user can now:

    1. Make online reservations for tickets (a maximum of 5) before buying them

    2. Add a profile picture – store them either in the database (bad practice) or in files on disk

2. Application Constraints

* The data will be stored in a relational database (and optionally in files on disk).

* Use the MVC architectural pattern to organize your application.

** Use **

* a domain logic pattern (transaction script or domain model) or a data source hybrid pattern (table module, active record)

* and a data source pure pattern (table data gateway, row data gateway, data mapper) most suitable for the application.

* Use Factory Method as stated above.

3. Requirements

* Create the analysis and design document (see the template).

* Implement and test the application.

4. Deliverables

* Analysis and design document.

* Implementation source files.

### Ruby App ###

A ticketing system mimicking the java application with a factory design pattern for festivals which are made of events which have sessions

### OpenGLSceneOfObjects ###

School project for the graphical processing system course (my work is only in the OpenGLApplication.cpp and modeling the objects and textures )libraries and any other extensions are a code skeleton received from the professor