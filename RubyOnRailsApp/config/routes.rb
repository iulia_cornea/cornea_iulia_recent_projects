Rails.application.routes.draw do
  devise_for :employees
  root 'static_pages#index'
  resources :clients

  post "/create-client", to: "clients#create", as: "create_client"
  post "/update-client", to: "clients#update", as: "update_client"

# resource <-- face linkurile (routes) pentru CRUD pentru vacations
# corelat cu controllerul de care apartine (vacations_controller)

  resources :vacations
# tip http request "/url-name", to: "nume_controller#nume_metoda",
# as: "cum_ma_refer_la_link" (pt redirect_to)
  post "/create-vacation", to: "vacations#create", as: "create_vacation"
  post "/update-vacation", to: "vacations#update", as: "update_vacation"

  resources :sight_seeings

  post "create_sight_seeing", to: "sight_seeings#create", as "create_sight_seeing"
  post "update_sight_seeing", to: "sight_seeings#update", as "update_sight_seeing"
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
