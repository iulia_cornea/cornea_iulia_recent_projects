class ClientsController < ApplicationController
  #CRUD on clients
  # pagini simple care nu fac nimic = GET

  before_action :authenticate_employee!

  def show
    @client = Client.find(params[:id])
  end

  # it's a GET <- afiseaza formularul
  def index
    @clients = Client.all
  end

  def new
    @client = Client.new
  end

  #it's a POST <- trimite ce a primit de la new la baza de date
  def create
    @client = Client.new(client_params)
    if @client.save
      flash[:notice]="Client adaugat cu succes."
      redirect_to clients_path
    else
      flash[:error]="Cet cliente n'a pas ete cree."
      redirect_to :back
    end
  end

  def edit
    @client = Client.find(params[:id])
  end

  def update
    @client = Client.find(client_params[:id])
    if @client.update(client_params)
      flash[:notice]="Client modificat cu succes."
      redirect_to client_path(@client)
    else
      flash[:error]="Cet cliente n'a pas ete modifier."
      redirect_to :back
    end
  end

  def destroy
    Client.find(params[:id]).destroy
    flash[:notice]="Destroyed!!!"
    redirect_to clients_path
  end


  private
  def client_params
    params.require(:client).permit(:name, :email, :picture, :id)
  end
end
