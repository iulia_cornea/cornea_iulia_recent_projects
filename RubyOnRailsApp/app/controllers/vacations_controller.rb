class VacationsController < ApplicationController

    before_action :authenticate_employee!, except: [:index, :show]

  def index
    @vacations = Vacation.all
  end

  def show
    @vacation = Vacation.find(params[:id])
  end


  # apelata ca sa afiseze formularul
  def new
    @vacation = Vacation.new
  end

  def create
    @vacation = Vacation.new(vacation_params)
    if @vacation.save
      flash[:notice]="Successfully added new vacation."
      redirect_to vacations_path
    else
      flash[:error]="Could not create vacation."
      redirect_to :back
    end
  end

  def edit
    @vacation = Vacation.find(params[:id])
  end

  def update
    @vacation = Vacation.find(vacation_params[:id])
    if @vacation.update(vacation_params)
      flash[:notice]="Vacation modified."
      redirect_to vacation_path(@vacation)
    else
      flash[:error]="Vacation was NOT modified."
      redirect_to :back
    end
  end

  def destroy
    Vacation.find(params[:id]).destroy
    flash[:notice]="Destroyed!!!"
    redirect_to vacations_path
  end

  private
  def vacation_params
    params.require(:vacation).permit(:vacation_type, :location, :destination, :start_date, :end_date, :price, :id)
  end
end
