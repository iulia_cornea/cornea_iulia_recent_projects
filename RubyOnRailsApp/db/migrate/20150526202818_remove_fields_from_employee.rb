class RemoveFieldsFromEmployee < ActiveRecord::Migration
  def change
    remove_column :employees, :email, :string
    remove_column :employees, :password, :string
  end
end
