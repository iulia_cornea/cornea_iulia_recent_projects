class CreateSightSeeings < ActiveRecord::Migration
  def change
    create_table :sight_seeings do |t|
      t.string :destination
      t.float :price

      t.timestamps null: false
    end
  end
end
