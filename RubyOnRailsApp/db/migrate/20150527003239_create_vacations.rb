class CreateVacations < ActiveRecord::Migration
  def change
    create_table :vacations do |t|
      t.string :type
      t.string :destination
      t.string :location
      t.date :start_date
      t.date :end_date
      t.float :price

      t.timestamps null: false
    end
  end
end
