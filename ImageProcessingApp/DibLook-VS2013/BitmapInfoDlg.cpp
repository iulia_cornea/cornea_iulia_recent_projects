// BitmapInfoDlg.cpp : implementation file
//

#include "stdafx.h"
#include "diblook.h"
#include "BitmapInfoDlg.h"
#include "afxdialogex.h"


// CBitmapInfoDlg dialog

IMPLEMENT_DYNAMIC(CBitmapInfoDlg, CDialog)

CBitmapInfoDlg::CBitmapInfoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CBitmapInfoDlg::IDD, pParent)
	, m_LUT(_T(""))
	, m_BMP_ver(_T(""))
	, m_HeaderSize(_T(""))
	, m_BitsPixel(_T(""))
	, m_Width(_T(""))
	, m_Height(_T(""))
	, m_LUTSize(_T(""))
{

}

CBitmapInfoDlg::~CBitmapInfoDlg()
{
}

void CBitmapInfoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_LUT);
	DDX_Text(pDX, ID_BMP_ver, m_BMP_ver);
	DDX_Text(pDX, ID_HeaderSize, m_HeaderSize);
	DDX_Text(pDX, ID_BitsPixel, m_BitsPixel);
	DDX_Text(pDX, ID_Width, m_Width);
	DDX_Text(pDX, ID_Height, m_Height);
	DDX_Text(pDX, ID_LUTSize, m_LUTSize);
}


BEGIN_MESSAGE_MAP(CBitmapInfoDlg, CDialog)
END_MESSAGE_MAP()


// CBitmapInfoDlg message handlers
