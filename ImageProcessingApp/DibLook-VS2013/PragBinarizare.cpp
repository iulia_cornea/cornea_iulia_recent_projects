// PragBinarizare.cpp : implementation file
//

#include "stdafx.h"
#include "diblook.h"
#include "PragBinarizare.h"
#include "afxdialogex.h"


// CPragBinarizare dialog

IMPLEMENT_DYNAMIC(CPragBinarizare, CDialog)

CPragBinarizare::CPragBinarizare(CWnd* pParent /*=NULL*/)
	: CDialog(CPragBinarizare::IDD, pParent)
	, m_thresh(BYTE(""))
{

}

CPragBinarizare::~CPragBinarizare()
{
}

void CPragBinarizare::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_thresh);
}


BEGIN_MESSAGE_MAP(CPragBinarizare, CDialog)
END_MESSAGE_MAP()


// CPragBinarizare message handlers
