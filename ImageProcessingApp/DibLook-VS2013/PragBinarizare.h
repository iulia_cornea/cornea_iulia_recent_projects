#pragma once


// CPragBinarizare dialog

class CPragBinarizare : public CDialog
{
	DECLARE_DYNAMIC(CPragBinarizare)

public:
	CPragBinarizare(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPragBinarizare();

// Dialog Data
	enum { IDD = IDD_DIALOG2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	BYTE m_thresh;
};
