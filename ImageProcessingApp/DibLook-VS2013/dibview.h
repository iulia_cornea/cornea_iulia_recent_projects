// dibview.h : interface of the CDibView class
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1998 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

class CDibView : public CScrollView
{
protected: // create from serialization only
	CDibView();
	DECLARE_DYNCREATE(CDibView)

// Attributes
public:
	CDibDoc* GetDocument()
		{
			ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CDibDoc)));
			return (CDibDoc*) m_pDocument;
		}

// Operations
public:

// Implementation
public:
	virtual ~CDibView();
	virtual void OnDraw(CDC* pDC);  // overridden to draw this view

	virtual void OnInitialUpdate();
	virtual void OnActivateView(BOOL bActivate, CView* pActivateView,
					CView* pDeactiveView);

	// Printing support
protected:
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);

// Generated message map functions
protected:
	//{{AFX_MSG(CDibView)
	afx_msg void OnEditCopy();
	afx_msg void OnUpdateEditCopy(CCmdUI* pCmdUI);
	afx_msg void OnEditPaste();
	afx_msg void OnUpdateEditPaste(CCmdUI* pCmdUI);
	afx_msg LRESULT OnDoRealize(WPARAM wParam, LPARAM lParam);  // user message
	afx_msg void OnProcessingParcurgereSimpla();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnProcessingPseudocolorare();
	afx_msg void OnProcessingAfisarebitmapinfo();
	afx_msg void OnProcessingGrayscale();
	afx_msg void OnProcessingBinarizarecuprag();
	afx_msg void OnProcessingHistograma();
	afx_msg void OnProcessingReducereniveluridegri();
	afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
	afx_msg void OnProcessingEtichetare();
	afx_msg void OnProcessingParcurgerecontur();
	afx_msg void OnProcessingReconstructiecontur();
	afx_msg void OnProcessingDilatare();
	afx_msg void OnProcessingE();
	afx_msg void OnProcessingDeschidere();
	afx_msg void OnProcessingInchidere();
	afx_msg void OnProcessingExtragerecontur();
	afx_msg void OnProcessingBinarizarecupragautomat();
	afx_msg void OnProcessingHistogramaluminozitate();
	afx_msg void OnProcessingEgalizarehistograma();
	afx_msg void OnProcessingEroziune();
	afx_msg void OnConvolutieMediearitmetica();
	afx_msg void OnConvolutieFiltrugaussian();
	afx_msg void OnConvolutieFiltrulaplace();
	afx_msg void OnConvolutieFouriernochange();
	afx_msg void OnConvolutieFouriertrece();
	afx_msg void OnConvolutieFouriermodul();
	afx_msg void OnConvolutieFouriermodulculog();
	afx_msg void OnFiltrareFiltrudeminim();
	afx_msg void OnFiltrareFiltrudemaxim();
	afx_msg void OnFiltrareFiltrudemedie();
	afx_msg void OnFiltrareFiltrugaussian();
	afx_msg void OnFiltrareFiltrugaussianseparat();
	afx_msg void OnEdgedetectionCanny();
	afx_msg void OnProiectGaussianratio();
	afx_msg void OnProiectLpsrc();
	afx_msg void OnProiectLpblured();
	afx_msg void OnProiectLpsrc32807();
	afx_msg void OnSscHexainfisier();
};

/////////////////////////////////////////////////////////////////////////////
