// dibview.cpp : implementation of the CDibView class
//
// This is a part of the Microsoft Foundation Classes C++ library.
// Copyright (C) 1992-1998 Microsoft Corporation
// All rights reserved.
//
// This source code is only intended as a supplement to the
// Microsoft Foundation Classes Reference and related
// electronic documentation provided with the library.
// See these sources for detailed information regarding the
// Microsoft Foundation Classes product.

#include "stdafx.h"
#include "diblook.h"

#include "dibdoc.h"
#include "dibview.h"
#include "dibapi.h"
#include "mainfrm.h"

#include "HRTimer.h"

//lab2 bitmap info dialog
#include "BitmapInfoDlg.h"
//lab2 casuta prag binarizare
#include "PragBinarizare.h"
//lab3 dialog box pentru afisare histograma
#include "DlgHistogram.h"
//lab9 fourier
#include "dibfft.h"
#include <math.h>
#define PI 3.14159265359

#ifdef _DEBUG
#undef THIS_FILE
static char BASED_CODE THIS_FILE[] = __FILE__;
#endif

#define BEGIN_PROCESSING() INCEPUT_PRELUCRARI()

#define END_PROCESSING(Title) SFARSIT_PRELUCRARI(Title)

#define INCEPUT_PRELUCRARI() \
	CDibDoc* pDocSrc=GetDocument();										\
	CDocTemplate* pDocTemplate=pDocSrc->GetDocTemplate();				\
	CDibDoc* pDocDest=(CDibDoc*) pDocTemplate->CreateNewDocument();		\
	BeginWaitCursor();													\
	HDIB hBmpSrc=pDocSrc->GetHDIB();									\
	HDIB hBmpDest = (HDIB)::CopyHandle((HGLOBAL)hBmpSrc);				\
	if ( hBmpDest==0 ) {												\
		pDocTemplate->RemoveDocument(pDocDest);							\
		return;															\
	}																	\
	BYTE* lpD = (BYTE*)::GlobalLock((HGLOBAL)hBmpDest);					\
	BYTE* lpS = (BYTE*)::GlobalLock((HGLOBAL)hBmpSrc);					\
	int iColors = DIBNumColors((char *)&(((LPBITMAPINFO)lpD)->bmiHeader)); \
	RGBQUAD *bmiColorsDst = ((LPBITMAPINFO)lpD)->bmiColors;	\
	RGBQUAD *bmiColorsSrc = ((LPBITMAPINFO)lpS)->bmiColors;	\
	BYTE * lpDst = (BYTE*)::FindDIBBits((LPSTR)lpD);	\
	BYTE * lpSrc = (BYTE*)::FindDIBBits((LPSTR)lpS);	\
	int dwWidth  = ::DIBWidth((LPSTR)lpS);\
	int dwHeight = ::DIBHeight((LPSTR)lpS);\
	int w=WIDTHBYTES(dwWidth*((LPBITMAPINFOHEADER)lpS)->biBitCount);	\
	HRTimer my_timer;	\
	my_timer.StartTimer();	\

#define BEGIN_SOURCE_PROCESSING \
	CDibDoc* pDocSrc=GetDocument();										\
	BeginWaitCursor();													\
	HDIB hBmpSrc=pDocSrc->GetHDIB();									\
	BYTE* lpS = (BYTE*)::GlobalLock((HGLOBAL)hBmpSrc);					\
	int iColors = DIBNumColors((char *)&(((LPBITMAPINFO)lpS)->bmiHeader)); \
	RGBQUAD *bmiColorsSrc = ((LPBITMAPINFO)lpS)->bmiColors;	\
	BYTE * lpSrc = (BYTE*)::FindDIBBits((LPSTR)lpS);	\
	int dwWidth  = ::DIBWidth((LPSTR)lpS);\
	int dwHeight = ::DIBHeight((LPSTR)lpS);\
	int w=WIDTHBYTES(dwWidth*((LPBITMAPINFOHEADER)lpS)->biBitCount);	\
	


#define END_SOURCE_PROCESSING	\
	::GlobalUnlock((HGLOBAL)hBmpSrc);								\
    EndWaitCursor();												\
/////////////////////////////////////////////////////////////////////////////


//---------------------------------------------------------------
#define SFARSIT_PRELUCRARI(Titlu)	\
	double elapsed_time_ms = my_timer.StopTimer();	\
	CString Title;	\
	Title.Format(_TEXT("%s - Proc. time = %.2f ms"), _TEXT(Titlu), elapsed_time_ms);	\
	::GlobalUnlock((HGLOBAL)hBmpDest);								\
	::GlobalUnlock((HGLOBAL)hBmpSrc);								\
    EndWaitCursor();												\
	pDocDest->SetHDIB(hBmpDest);									\
	pDocDest->InitDIBData();										\
	pDocDest->SetTitle((LPCTSTR)Title);									\
	CFrameWnd* pFrame=pDocTemplate->CreateNewFrame(pDocDest,NULL);	\
	pDocTemplate->InitialUpdateFrame(pFrame,pDocDest);	\

/////////////////////////////////////////////////////////////////////////////
// CDibView

IMPLEMENT_DYNCREATE(CDibView, CScrollView)

BEGIN_MESSAGE_MAP(CDibView, CScrollView)
	//{{AFX_MSG_MAP(CDibView)
	ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
	ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateEditCopy)
	ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
	ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdateEditPaste)
	ON_MESSAGE(WM_DOREALIZE, OnDoRealize)
	ON_COMMAND(ID_PROCESSING_PARCURGERESIMPLA, OnProcessingParcurgereSimpla)
	//}}AFX_MSG_MAP

	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CScrollView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CScrollView::OnFilePrintPreview)
	ON_COMMAND(ID_PROCESSING_PSEUDOCOLORARE, &CDibView::OnProcessingPseudocolorare)
	ON_COMMAND(ID_PROCESSING_AFISAREBITMAPINFO, &CDibView::OnProcessingAfisarebitmapinfo)
	ON_COMMAND(ID_PROCESSING_GRAYSCALE, &CDibView::OnProcessingGrayscale)
	ON_COMMAND(ID_PROCESSING_BINARIZARECUPRAG, &CDibView::OnProcessingBinarizarecuprag)
	ON_COMMAND(ID_PROCESSING_HISTOGRAMA, &CDibView::OnProcessingHistograma)
	ON_COMMAND(ID_PROCESSING_REDUCERENIVELURIDEGRI, &CDibView::OnProcessingReducereniveluridegri)
	ON_WM_LBUTTONDBLCLK()
	ON_COMMAND(ID_PROCESSING_ETICHETARE, &CDibView::OnProcessingEtichetare)
	ON_COMMAND(ID_PROCESSING_PARCURGERECONTUR, &CDibView::OnProcessingParcurgerecontur)
	ON_COMMAND(ID_PROCESSING_RECONSTRUCTIECONTUR, &CDibView::OnProcessingReconstructiecontur)
	ON_COMMAND(ID_PROCESSING_DILATARE, &CDibView::OnProcessingDilatare)
	ON_COMMAND(ID_PROCESSING_E, &CDibView::OnProcessingE)
	ON_COMMAND(ID_PROCESSING_DESCHIDERE, &CDibView::OnProcessingDeschidere)
	ON_COMMAND(ID_PROCESSING_INCHIDERE, &CDibView::OnProcessingInchidere)
	ON_COMMAND(ID_PROCESSING_EXTRAGERECONTUR, &CDibView::OnProcessingExtragerecontur)
	ON_COMMAND(ID_PROCESSING_BINARIZARECUPRAGAUTOMAT, &CDibView::OnProcessingBinarizarecupragautomat)
	ON_COMMAND(ID_PROCESSING_HISTOGRAMALUMINOZITATE, &CDibView::OnProcessingHistogramaluminozitate)
	ON_COMMAND(ID_PROCESSING_EGALIZAREHISTOGRAMA, &CDibView::OnProcessingEgalizarehistograma)
	ON_COMMAND(ID_PROCESSING_EROZIUNE, &CDibView::OnProcessingEroziune)
	ON_COMMAND(ID_CONVOLUTIE_MEDIEARITMETICA, &CDibView::OnConvolutieMediearitmetica)
	ON_COMMAND(ID_CONVOLUTIE_FILTRUGAUSSIAN, &CDibView::OnConvolutieFiltrugaussian)
	ON_COMMAND(ID_CONVOLUTIE_FILTRULAPLACE, &CDibView::OnConvolutieFiltrulaplace)
	ON_COMMAND(ID_CONVOLUTIE_FOURIERNOCHANGE, &CDibView::OnConvolutieFouriernochange)
	ON_COMMAND(ID_CONVOLUTIE_FOURIERTRECE, &CDibView::OnConvolutieFouriertrece)
	ON_COMMAND(ID_CONVOLUTIE_FOURIERMODUL, &CDibView::OnConvolutieFouriermodul)
	ON_COMMAND(ID_CONVOLUTIE_FOURIERMODULCULOG, &CDibView::OnConvolutieFouriermodulculog)
	ON_COMMAND(ID_FILTRARE_FILTRUDEMINIM, &CDibView::OnFiltrareFiltrudeminim)
	ON_COMMAND(ID_FILTRARE_FILTRUDEMAXIM, &CDibView::OnFiltrareFiltrudemaxim)
	ON_COMMAND(ID_FILTRARE_FILTRUDEMEDIE, &CDibView::OnFiltrareFiltrudemedie)
	ON_COMMAND(ID_FILTRARE_FILTRUGAUSSIAN, &CDibView::OnFiltrareFiltrugaussian)
	ON_COMMAND(ID_FILTRARE_FILTRUGAUSSIANSEPARAT, &CDibView::OnFiltrareFiltrugaussianseparat)
	ON_COMMAND(ID_EDGEDETECTION_CANNY, &CDibView::OnEdgedetectionCanny)
	ON_COMMAND(ID_PROIECT_GAUSSIANRATIO, &CDibView::OnProiectGaussianratio)
	ON_COMMAND(ID_PROIECT_LPSRC, &CDibView::OnProiectLpsrc)
	ON_COMMAND(ID_PROIECT_LPBLURED, &CDibView::OnProiectLpblured)
	ON_COMMAND(ID_PROIECT_LPSRC32807, &CDibView::OnProiectLpsrc32807)
	ON_COMMAND(ID_SSC_HEXAINFISIER, &CDibView::OnSscHexainfisier)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDibView construction/destruction

CDibView::CDibView()
{
}

CDibView::~CDibView()
{
}

/////////////////////////////////////////////////////////////////////////////
// CDibView drawing

void CDibView::OnDraw(CDC* pDC)
{
	CDibDoc* pDoc = GetDocument();

	HDIB hDIB = pDoc->GetHDIB();
	if (hDIB != NULL)
	{
		LPSTR lpDIB = (LPSTR) ::GlobalLock((HGLOBAL) hDIB);
		int cxDIB = (int) ::DIBWidth(lpDIB);         // Size of DIB - x
		int cyDIB = (int) ::DIBHeight(lpDIB);        // Size of DIB - y
		::GlobalUnlock((HGLOBAL) hDIB);
		CRect rcDIB;
		rcDIB.top = rcDIB.left = 0;
		rcDIB.right = cxDIB;
		rcDIB.bottom = cyDIB;
		CRect rcDest;
		if (pDC->IsPrinting())   // printer DC
		{
			// get size of printer page (in pixels)
			int cxPage = pDC->GetDeviceCaps(HORZRES);
			int cyPage = pDC->GetDeviceCaps(VERTRES);
			// get printer pixels per inch
			int cxInch = pDC->GetDeviceCaps(LOGPIXELSX);
			int cyInch = pDC->GetDeviceCaps(LOGPIXELSY);

			//
			// Best Fit case -- create a rectangle which preserves
			// the DIB's aspect ratio, and fills the page horizontally.
			//
			// The formula in the "->bottom" field below calculates the Y
			// position of the printed bitmap, based on the size of the
			// bitmap, the width of the page, and the relative size of
			// a printed pixel (cyInch / cxInch).
			//
			rcDest.top = rcDest.left = 0;
			rcDest.bottom = (int)(((double)cyDIB * cxPage * cyInch)
					/ ((double)cxDIB * cxInch));
			rcDest.right = cxPage;
		}
		else   // not printer DC
		{
			rcDest = rcDIB;
		}
		::PaintDIB(pDC->m_hDC, &rcDest, pDoc->GetHDIB(),
			&rcDIB, pDoc->GetDocPalette());
	}
}

/////////////////////////////////////////////////////////////////////////////
// CDibView printing

BOOL CDibView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

/////////////////////////////////////////////////////////////////////////////
// CDibView commands


LRESULT CDibView::OnDoRealize(WPARAM wParam, LPARAM)
{
	ASSERT(wParam != NULL);
	CDibDoc* pDoc = GetDocument();
	if (pDoc->GetHDIB() == NULL)
		return 0L;  // must be a new document

	CPalette* pPal = pDoc->GetDocPalette();
	if (pPal != NULL)
	{
		CMainFrame* pAppFrame = (CMainFrame*) AfxGetApp()->m_pMainWnd;
		ASSERT_KINDOF(CMainFrame, pAppFrame);

		CClientDC appDC(pAppFrame);
		// All views but one should be a background palette.
		// wParam contains a handle to the active view, so the SelectPalette
		// bForceBackground flag is FALSE only if wParam == m_hWnd (this view)
		CPalette* oldPalette = appDC.SelectPalette(pPal, ((HWND)wParam) != m_hWnd);

		if (oldPalette != NULL)
		{
			UINT nColorsChanged = appDC.RealizePalette();
			if (nColorsChanged > 0)
				pDoc->UpdateAllViews(NULL);
			appDC.SelectPalette(oldPalette, TRUE);
		}
		else
		{
			TRACE0("\tSelectPalette failed in CDibView::OnPaletteChanged\n");
		}
	}

	return 0L;
}

void CDibView::OnInitialUpdate()
{
	CScrollView::OnInitialUpdate();
	ASSERT(GetDocument() != NULL);

	SetScrollSizes(MM_TEXT, GetDocument()->GetDocSize());
}


void CDibView::OnActivateView(BOOL bActivate, CView* pActivateView,
					CView* pDeactiveView)
{
	CScrollView::OnActivateView(bActivate, pActivateView, pDeactiveView);

	if (bActivate)
	{
		ASSERT(pActivateView == this);
		OnDoRealize((WPARAM)m_hWnd, 0);   // same as SendMessage(WM_DOREALIZE);
	}
}

void CDibView::OnEditCopy()
{
	CDibDoc* pDoc = GetDocument();
	// Clean clipboard of contents, and copy the DIB.

	if (OpenClipboard())
	{
		BeginWaitCursor();
		EmptyClipboard();
		SetClipboardData (CF_DIB, CopyHandle((HANDLE) pDoc->GetHDIB()) );
		CloseClipboard();
		EndWaitCursor();
	}
}



void CDibView::OnUpdateEditCopy(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(GetDocument()->GetHDIB() != NULL);
}


void CDibView::OnEditPaste()
{
	HDIB hNewDIB = NULL;

	if (OpenClipboard())
	{
		BeginWaitCursor();

		hNewDIB = (HDIB) CopyHandle(::GetClipboardData(CF_DIB));

		CloseClipboard();

		if (hNewDIB != NULL)
		{
			CDibDoc* pDoc = GetDocument();
			pDoc->ReplaceHDIB(hNewDIB); // and free the old DIB
			pDoc->InitDIBData();    // set up new size & palette
			pDoc->SetModifiedFlag(TRUE);

			SetScrollSizes(MM_TEXT, pDoc->GetDocSize());
			OnDoRealize((WPARAM)m_hWnd,0);  // realize the new palette
			pDoc->UpdateAllViews(NULL);
		}
		EndWaitCursor();
	}
}


void CDibView::OnUpdateEditPaste(CCmdUI* pCmdUI)
{
	pCmdUI->Enable(::IsClipboardFormatAvailable(CF_DIB));
}

void CDibView::OnProcessingParcurgereSimpla() 
{
	// TODO: Add your command handler code here
	BEGIN_PROCESSING();

	// Makes a grayscale image by equalizing the R, G, B components from the LUT
	for (int k=0;  k < iColors ; k++)
		bmiColorsDst[k].rgbRed=bmiColorsDst[k].rgbGreen=bmiColorsDst[k].rgbBlue=k;

	//  Goes through the bitmap pixels and performs their negative	
	for (int i=0;i<dwHeight;i++)
		for (int j=0;j<dwWidth;j++)
		  {	
			lpDst[i*w+j]= 255 - lpSrc[i*w+j]; //makes image negative
	  }

	END_PROCESSING("Negativ imagine");
}



void CDibView::OnProcessingPseudocolorare()
{
	BEGIN_PROCESSING();

	for (int k = 0; k < iColors; k = k + 2){
		bmiColorsDst[k].rgbRed = 100 + k;
		bmiColorsDst[k].rgbGreen = iColors - k;
		bmiColorsDst[k].rgbBlue = k;
	}

	int r = w / 6, ic = dwHeight / 2, jc = w / 2;


	for (int i = 0; i<dwHeight; i++)
	for (int j = 0; j<dwWidth; j++)
	{
		if ((i - ic)*(i - ic) + (j - jc)*(j - jc) >= r*r)
		if (lpDst[i*w + j] % 2 == 0)
			lpDst[i*w + j] = lpSrc[i*w + j] + 1;
		else
			lpDst[i*w + j] = lpSrc[i*w + j];
		else
		{
			if (lpDst[i*w + j] % 2 == 0)
				lpDst[i*w + j] = lpDst[i*w + j];
			else
				lpDst[i*w + j] = lpDst[i*w + j] + 1;
		}

	}

	END_PROCESSING("Pseudocolorare imagine");
}


void CDibView::OnProcessingAfisarebitmapinfo()
{
	BEGIN_SOURCE_PROCESSING;
	CBitmapInfoDlg dlgBmpHeader;

	LPBITMAPINFO pBitmapInfoSrc = (LPBITMAPINFO)lpS;
	dlgBmpHeader.m_BMP_ver.Format(_TEXT("BMP version: %d"),0);
	dlgBmpHeader.m_HeaderSize.Format(_TEXT("Header Size: %d"), pBitmapInfoSrc->bmiHeader.biSize);
	dlgBmpHeader.m_BitsPixel.Format(_TEXT("Bits per pixel: %d"), pBitmapInfoSrc->bmiHeader.biBitCount);
	dlgBmpHeader.m_Width.Format(_TEXT("Image width [pixels]: %d"), pBitmapInfoSrc->bmiHeader.biWidth);
	dlgBmpHeader.m_Height.Format(_TEXT("Image height [pixels]: %d"), pBitmapInfoSrc->bmiHeader.biHeight);
	dlgBmpHeader.m_LUTSize.Format(_TEXT("LUT Size [pixels]: %d"),0);

	CString buffer;
	for (int i = 0; i<iColors; i++)
	{
		buffer.Format(_TEXT("%3d.\t%3d\t%3d\t%3d\r\n"), i,
			bmiColorsSrc[i].rgbRed,
			bmiColorsSrc[i].rgbGreen,
			bmiColorsSrc[i].rgbBlue);
		dlgBmpHeader.m_LUT += buffer;
	}

	dlgBmpHeader.DoModal();
	END_SOURCE_PROCESSING;
}


void CDibView::OnProcessingGrayscale()
{
	BEGIN_PROCESSING();
	int red, green, blue, intensity, g[256];
	if (iColors == 0)
	{
		for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
		{
			red = lpSrc[i*w + j * 3 + 2];
			green = lpSrc[i*w + j * 3 + 1];
			blue = lpSrc[i*w + j * 3 + 0];

			intensity = (red + green + blue) / 3;
			lpDst[i*w + j * 3 + 2] = intensity;
			lpDst[i*w + j * 3 + 1] = intensity;
			lpDst[i*w + j * 3 + 0] = intensity;
		}
	}
	else if (iColors == 256)
	{
		for (int i = 0; i < 256; i++)
			g[i] = (bmiColorsSrc[i].rgbRed + bmiColorsSrc[i].rgbGreen + bmiColorsSrc[i].rgbBlue) / 3;


		for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
			lpDst[i*w + j] = g[lpSrc[i*w + j]];

		for (int i = 0; i < 256; i++)
			bmiColorsDst[i].rgbRed = bmiColorsDst[i].rgbGreen = bmiColorsDst[i].rgbBlue = i;

	}

	else perror("Not 8 nor 24.");

	END_PROCESSING("Gray Scale Image");
}


void CDibView::OnProcessingBinarizarecuprag()
{
	BYTE threshold;

	CPragBinarizare dlgThresh;
	if (dlgThresh.DoModal() == IDOK)
	{
		threshold = dlgThresh.m_thresh;
		BEGIN_PROCESSING();
		for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
		{
			if (lpSrc[i*w + j] < threshold)
			{
				lpDst[i*w + j] = 0;
			}
			else
			{
				lpDst[i*w + j] = 255;
			}
		}
		END_PROCESSING("Binarizare imagine");
	}
}


void CDibView::OnProcessingHistograma()
{
	BEGIN_SOURCE_PROCESSING;
	
	int histValues[256];
		
	for (int i = 0; i < 256; i++)
	{
		histValues[i] = 0;
	}

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
		histValues[lpSrc[i*w + j]] ++;
	
	CDlgHistogram dlg;
	memcpy(dlg.m_Histogram.values, histValues, sizeof(histValues));
	dlg.DoModal();

	END_SOURCE_PROCESSING;
}


void CDibView::OnProcessingReducereniveluridegri()
{
	BEGIN_PROCESSING();

	int histValues[256];
	float FDPValues[256];
	int localMax[256];
	int praguri[256];
	float m = dwHeight*dwWidth;
	int WH = 5;
	float TH = 0.0003;

	for (int i = 0; i < 256; i++)
	{
		histValues[i] = 0;
		localMax[i] = -1;
		praguri[i] = -1;
	}
	//caluculare histograma
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
		histValues[lpSrc[i*w + j]] ++;

	//normalize histogram
	for (int i = 0; i < 256; i++)
		FDPValues[i] = (float)histValues[i] / m;

	//parcurgere FDPValues cu fereastra ed 2*WH+1
	localMax[0] = 0;
	localMax[255] = 255;
	float v;
	int ok;
	for (int k = 0 + WH; k < 256 - WH - 1; k++)
	{
		v = 0.0;
		//calulare medie valoarea v
		for (int i = k - WH; i <= k + WH; i++)
		{
			v = v + FDPValues[i];
		}
		v = v / (2 * WH + 1);
		if (FDPValues[k] > v + TH)
		{
			ok = 1;
			for (int i = k - WH; i <= k + WH; i++)
			{
				if (FDPValues[i] > FDPValues[k])
				{
					ok = 0;
				}
			}

		}
		if (ok == 1)
		{
			localMax[k] = k;
		}
	}

	//determinare praguri
	int contor = 1;
	int distanta = 0;
	int prag;
	while (contor < 256)
	{
		if(localMax[contor] == -1)
		{
			distanta++;
		}
		else
		{
			prag = (contor - distanta + contor) / 2;
			praguri[prag] = prag;
			distanta = 0;
		}
		contor++;
	}
	int intensitate;
	//reducere niveluri de gri 
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		intensitate = lpSrc[i*w + j];
		//gasire prag
		while (praguri[intensitate] == -1)
		{
			intensitate++;
		}
		while (localMax[intensitate] == -1)
		{
			intensitate--;
		}
		lpDst[i*w + j] = intensitate; 
	}
	END_PROCESSING("Reducere Niveluri de Gri");
}


void CDibView::OnLButtonDblClk(UINT nFlags, CPoint point)
{
	BEGIN_PROCESSING();
	//poz curenta
	CPoint pos = GetScrollPosition() + point;

	int obiect = 0, fundal = 255;
	int arie = 0, perimetru = 0, ok;
	//schimbare coord in coord img
	int j = pos.x;
	int i = dwHeight - pos.y - 1;

	float ri = 0, ci = 0;
	float teta;
	float numarator = 0, numitor1 = 0, numitor2 = 0;


	obiect = lpSrc[i*w + j];

	for (int ii = 0; ii < dwHeight; ii++)
	for (int jj = 0; jj < dwWidth; jj++)
	{
		if (lpSrc[ii*w + jj] == obiect)
		{
			//arie
			arie += 1;
			//perimetru
			ok = 0;
			for (int ki = ii - 1; ki <= ii + 1; ki++)
			for (int kj = jj - 1; kj <= jj; kj++)
			if (lpSrc[ki*w + kj] != obiect)
				ok = 1;
			if (ok)
				perimetru += 1;
			//centru masa
			ri += ii;
			ci += jj;


		}


	}

	//centru de masa
	ri = ri / arie;
	ci = ci / arie;

	for (int ii = 0; ii < dwHeight; ii++)
	for (int jj = 0; jj < dwWidth; jj++)
	{
		if (lpSrc[ii*w + jj] == obiect)
		{
			numarator += (ii - ri)*(jj - ci);
			numitor1 += (jj - ci)*(jj - ci);
			numitor2 += (ii - ri)*(ii - ri);
		}
	}

	teta = atan2(2 * numarator, (numitor1 - numitor2));
	teta = teta / 2;

	//puncte linie
	float deltaI, deltaJ;
	int i1, j1, i2, j2;
	deltaI = 100 * sin(teta);
	deltaJ = 100 * cos(teta);
	i1 = ri + deltaI;
	j1 = ci + deltaJ;

	i2 = ri - deltaI;
	j2 = ci - deltaJ;


	if (j > 0 && j<dwWidth && i>0 && i < dwHeight){
		CString info;
		info.Format(_TEXT("i=%d, j=%d, color=%d, perimetru=%d, aria=%d\nci=%f, ri=%f, teta=%f, deltaI=%f,deltaJ=%f, numarator=%f, n1=%f, n2=%f"),
			i, j, lpSrc[i*w + j], perimetru, arie, ci, ri, teta, deltaI, deltaJ, numarator, numitor1, numitor2);
		AfxMessageBox(info);
	}

	//ob DC din memorie ... compatibil cu ecranul
	CDC dc;
	dc.CreateCompatibleDC(0);

	//contine DDB compatibil cu ecranul
	CBitmap ddBitmap;

	//initializare cu datele din img src
	HBITMAP hDDBitmap = CreateDIBitmap(::GetDC(0), &((LPBITMAPINFO)lpS)->bmiHeader, CBM_INIT, lpSrc, (LPBITMAPINFO)lpS, DIB_RGB_COLORS);

	ddBitmap.Attach(hDDBitmap);

	CBitmap* pTempBmp = dc.SelectObject(&ddBitmap);

	//incepand de aici toate desenarile se vor face pe imaginea DDB

	CPen pen(PS_SOLID, 1, RGB(0, 255, 0));
	CPen* pTempPen = dc.SelectObject(&pen);


	//desenare centru de masa
	dc.MoveTo(ci - 10, dwHeight - ri - 1 - 10);
	dc.LineTo(ci + 10, dwHeight - ri - 1 + 10);

	dc.MoveTo(ci + 10, dwHeight - ri - 1 - 10);
	dc.LineTo(ci - 10, dwHeight - ri - 1 + 10);

	//draw elongation
	dc.MoveTo(j1, dwHeight - i1 - 1);
	dc.LineTo(j2, dwHeight - i2 - 1);

	//select vechiul pen si vechiul bmp
	dc.SelectObject(pTempPen);
	dc.SelectObject(pTempBmp);

	//copiaza pixelii din DDB in DIB-ul destinatie
	GetDIBits(dc.m_hDC, ddBitmap, 0, dwHeight, lpDst, (LPBITMAPINFO)lpD, DIB_RGB_COLORS);

	int projection[700];
	for (int count = 0; count < 700; count++)
		projection[count] = 0;

	//compute Ox projection
	for (int jj = 0; jj < dwWidth; jj++)
	for (int ii = 0; ii < dwHeight; ii++)
	if (lpSrc[ii*w + jj] == obiect)
	{
		projection[jj] += 1;
	}
	for (int jj = 0; jj < dwWidth; jj++)
	for (int ii = 0; ii < projection[jj]; ii++)
		lpDst[ii*w + jj] = 0;

	END_PROCESSING("Trasaturi geometrice");
	CScrollView::OnLButtonDblClk(nFlags, point);
}


void CDibView::OnProcessingEtichetare()
{
	// TODO: Add your command handler code here
}


void CDibView::OnProcessingParcurgerecontur()
{
	BEGIN_PROCESSING();

	int fundal = 255;
	int obiect = 0;
	int ci, cj, ni, nj, dir, ndir;
	//parcurgere imagine de jos in sus
	int dx[] = { 1, 0, -1, 0 };
	int dy[] = { 0, -1, 0, 1 };
	//lista de directii
	int conturDir[1000];
	int conturI[1000];
	int conturJ[1000];
	int conturCounter = 0;
	//fisier
	FILE *pFile;
	pFile = fopen("D:\\School\\IP\\Cornea_Iulia_30231_IP\\directie.txt", "w");


	for (int i = 0; i < 1000; i++)
	{
		conturI[i] = conturJ[i] = conturDir[i] = -1;
	}

	int ok = 0;

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (lpSrc[i*w + j] == obiect)
		{
			ci = i;
			cj = j;
			dir = 0;
			break;
		}
	}

	conturCounter++;
	conturI[conturCounter] = ci;
	conturJ[conturCounter] = cj;
	conturDir[conturCounter] = dir;

	do{
		ndir = (dir + 3) % 4;

		ni = ci + dy[ndir];
		nj = cj + dx[ndir];

		while (lpSrc[ni*w + nj] == fundal)
		{
			ndir = (ndir + 1) % 4;
			ni = ci + dy[ndir];
			nj = cj + dx[ndir];
		}

		dir = ndir;
		ci = ni;
		cj = nj;

		conturCounter++;
		conturI[conturCounter] = ci;
		conturJ[conturCounter] = cj;
		conturDir[conturCounter] = dir;

	} while (!(conturI[1] == conturI[conturCounter] && conturJ[1] == conturJ[conturCounter]));

	for (int i = 0; i < iColors; i++)
	{
		bmiColorsDst[i].rgbBlue = bmiColorsDst[i].rgbGreen = bmiColorsDst[i].rgbRed = 255;
	}

	bmiColorsDst[1].rgbBlue = bmiColorsDst[1].rgbGreen = bmiColorsDst[1].rgbRed = 0;

	fprintf(pFile, "Directie pentru imagine:\n");
	for (int i = 1; i <= conturCounter; i++)
	{
		lpDst[conturI[i] * w + conturJ[i]] = 1;
		fprintf(pFile, "%d, ", conturDir[i]);
	}

	fprintf(pFile, "\nDerivata directiei pentru imagine:\n");
	for (int i = 1; i < conturCounter; i++)
	{
		fprintf(pFile, "%d, ", abs((conturDir[i + 1] - conturDir[i]) % 4));
	}

	fclose(pFile);

	END_PROCESSING("Contur");
}


void CDibView::OnProcessingReconstructiecontur()
{
	BEGIN_PROCESSING();

	int ci, cj, lungime, dir;
	int dj[] = { 1, 1, 0, -1, -1, -1, 0, 1 };
	//int di[] = { 0, -1, -1, -1, 0, 1, 1, 1 };
	int di[] = { 0, 1, 1, 1, 0, -1, -1, -1 };
	FILE *pFile;
	pFile = fopen("D:\\School\\IP\\Cornea_Iulia_30231_IP\\reconstruct.txt", "r");

	fscanf(pFile, "%d", &ci);
	fscanf(pFile, "%d", &cj);
	fscanf(pFile, "%d", &lungime);

	lpDst[ci*w + cj] = 0;
	for (int i = 1; i <= lungime; i++)
	{
		fscanf(pFile, "%d", &dir);
		ci = ci + di[dir];
		cj = cj + dj[dir];
		lpDst[ci*w + cj] = 1;
	}

	bmiColorsDst[1].rgbBlue = bmiColorsDst[1].rgbGreen = bmiColorsDst[1].rgbRed = 0;

	fclose(pFile);

	END_PROCESSING("Reconstructie Contur");
}


void dilatare(BYTE *lpSrc, int dwHeight, int dwWidth, int w, BYTE *lpDst)
{
	int negru = 0, alb = 255;
	int nucleu[3][3] = { { alb, negru, alb }, { negru, negru, negru }, { alb, negru, alb } };
	int nn = 3;
	int orgNi = 1, orgNj = 1;

	for (int i = nn; i < dwHeight - nn; i++)
	for (int j = nn; j < dwWidth - nn; j++)
	{
		if (lpSrc[i*w + j] == negru)
		{
			for (int ii = -nn / 2; ii <= nn / 2; ii++)
			for (int jj = -nn / 2; jj <= nn / 2; jj++)
			if (nucleu[ii + nn / 2][jj + nn / 2] == negru)
			{
				lpDst[(i + ii)*w + (j + jj)] = negru;
			}

		}
	}
}

void eroziune(BYTE *lpSrc, int dwHeight, int dwWidth, int w, BYTE *lpDst)
{
	int negru = 0, alb = 255;
	int nucleu[3][3] = { { alb, negru, alb }, { negru, negru, negru }, { alb, negru, alb } };
	int nn = 3;
	int orgNi = 1, orgNj = 1;

	for (int i = nn; i < dwHeight - nn; i++)
	for (int j = nn; j < dwWidth - nn; j++)
	{
		if (lpSrc[i*w + j] == negru)
		{
			int ok = false;
			for (int ii = -nn / 2; ii <= nn / 2; ii++)
			for (int jj = -nn / 2; jj <= nn / 2; jj++)
			if (nucleu[ii + nn / 2][jj + nn / 2] == negru && lpSrc[(i + ii)*w + (j + jj)] == alb)
			{
				ok = true;
			}
			if (ok)
			{
				for (int ii = -nn / 2; ii <= nn / 2; ii++)
				for (int jj = -nn / 2; jj <= nn / 2; jj++)
				if (lpSrc[(i + ii)*w + (j + jj)] == negru)
				{
					lpDst[(i + ii)*w + (j + jj)] = alb;
				}
			}
		}
	}
}

void CDibView::OnProcessingDilatare()
{
	BEGIN_PROCESSING();

	dilatare(lpSrc, dwHeight, dwWidth, w, lpDst);

	END_PROCESSING("Dilatare");
}


void CDibView::OnProcessingEroziune()
{
	BEGIN_PROCESSING();

	eroziune(lpSrc, dwHeight, dwWidth, w, lpDst);

	END_PROCESSING("Eroziune");
}

//made some mistake -- don not know for sure what
void CDibView::OnProcessingE()
{
}


void CDibView::OnProcessingDeschidere()
{
	BEGIN_PROCESSING();
	BYTE *lpTemp = new BYTE[w*dwHeight];
	memcpy(lpTemp, lpSrc, w*dwHeight);
	eroziune(lpSrc, dwHeight, dwWidth, w, lpTemp);
	memcpy(lpDst, lpTemp, w*dwHeight);
	dilatare(lpTemp, dwHeight, dwWidth, w, lpDst);
	END_PROCESSING("Deschidere");
}


void CDibView::OnProcessingInchidere()
{
	BEGIN_PROCESSING();
	unsigned char *lpTemp = new unsigned char[w*dwHeight];
	memcpy(lpTemp, lpSrc, w*dwHeight);
	dilatare(lpSrc, dwHeight, dwWidth, w, lpTemp);
	memcpy(lpDst, lpTemp, w*dwHeight);
	eroziune(lpTemp, dwHeight, dwWidth, w, lpDst);
	END_PROCESSING("Inchidere");
}


void CDibView::OnProcessingExtragerecontur()
{
	BEGIN_PROCESSING();
	int negru = 0;
	int alb = 255;
	unsigned char *lpTemp = new unsigned char[w*dwHeight];
	unsigned char *lpTemp2 = new unsigned char[w*dwHeight];
	memcpy(lpTemp, lpSrc, w*dwHeight);
	dilatare(lpSrc, dwHeight, dwWidth, w, lpTemp);
	memcpy(lpTemp2, lpTemp, w*dwHeight);
	eroziune(lpTemp, dwHeight, dwWidth, w, lpTemp2);
	// lpTemp2 = A erodat cu B

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (lpSrc[i*w + j] == negru && lpTemp2[i*w + j] == negru)
		{
			lpDst[i*w + j] = alb;
		}
	}

	END_PROCESSING("Extragere Contur");
}


void CDibView::OnProcessingBinarizarecupragautomat()
{
	BEGIN_PROCESSING();

	int histValues[256];
	int newHist[256];
	int oldHist[256];
	float m = dwHeight*dwWidth;
	float myu = 0;
	float sigma = 0;
	int functiaA[256];
	int maxA;
	int pragT;
	for (int i = 0; i < 256; i++)
	{
		histValues[i] = 0;
	}
	//Calcul histograma
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
		histValues[lpSrc[i*w + j]] ++;
	//Calcul medie
	for (int i = 0; i < 256; i++)
	{
		myu = myu + i*histValues[i];
	}
	myu = myu / m;
	//Calcul deviatie
	for (int i = 0; i < 256; i++)
	{
		sigma = sigma + (i - myu)*(i - myu)*histValues[i];
	}
	sigma = sqrt(sigma / m);

	//Calcul functia A pentru 0 -255 si pragT
	functiaA[0] = m;
	maxA = functiaA[0];
	pragT = 0;
	for (int i = 1; i < 256; i++)
	{
		functiaA[i] = functiaA[i - 1] - histValues[i - 1];
		if (i*functiaA[i]>maxA)
		{
			maxA = i*functiaA[i];
			pragT = i;
		}
	}

	CString info;
	info.Format(_TEXT("Media este: %f\nDeviatia este: %f\nPragul T este: %d"), myu, sigma, pragT);
	AfxMessageBox(info);

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	if (lpSrc[i*w + j]>pragT)
		lpDst[i*w + j] = 255;
	else
		lpDst[i*w + j] = 0;


	CDlgHistogram dlg;
	memcpy(dlg.m_Histogram.values, histValues, sizeof(histValues));
	dlg.DoModal();

	END_PROCESSING("Binarizare cu prag automat");
}


void CDibView::OnProcessingHistogramaluminozitate()
{
	BEGIN_PROCESSING();

	int histValues[256];
	int newHist[256];
	int oldHist[256];
	float m = dwHeight*dwWidth;
	float myuIn = 0;
	float sigmaIn = 0;
	float myuOut;
	float sigmaOut;
	float imin, omin;
	float imax, omax;
	int deltaMyu = 50;
	int deltaSigma = 50;

	for (int i = 0; i < 256; i++)
	{
		histValues[i] = 0;
	}
	//Calcul histograma
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
		histValues[lpSrc[i*w + j]] ++;
	//Calcul medie
	for (int i = 0; i < 256; i++)
	{
		myuIn = myuIn + i*histValues[i];
	}
	myuIn = myuIn / m;
	//Calcul deviatie
	for (int i = 0; i < 256; i++)
	{
		sigmaIn = sigmaIn + (i - myuIn)*(i - myuIn)*histValues[i];
	}
	sigmaIn = sqrt(sigmaIn / m);

	CString info;
	info.Format(_TEXT("Media este: %f\nDeviatia este: %f\n"), myuIn, sigmaIn);
	AfxMessageBox(info);

	imin = myuIn - sigmaIn;
	imax = myuIn + sigmaIn;

	myuOut = myuIn + deltaMyu;
	sigmaOut = sigmaIn + deltaSigma;

	omin = myuOut - sigmaOut;
	omax = myuOut + sigmaOut;

	float constanta = (omax - omin) / (imax - imin);

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (omin + (lpSrc[i*w + j] - imin)*constanta < 0)
		{
			lpDst[i*w + j] = 0;
		}
		else
		{
			if (omin + (lpSrc[i*w + j] - imin)*constanta > 255)
			{
				lpDst[i*w + j] = 255;
			}
			else
			{
				lpDst[i*w + j] = omin + (lpSrc[i*w + j] - imin)*constanta;
			}
		}
	}

	END_PROCESSING("Histograma stech/shrik");
}


void CDibView::OnProcessingEgalizarehistograma()
{
	BEGIN_PROCESSING();

	int histValues[256];
	float histpdf[256];
	float histCpdf[256];
	int newHist[256];
	int oldHist[256];
	float m = dwHeight*dwWidth;
	float myu = 0;
	float sigma = 0;

	for (int i = 0; i < 256; i++)
	{
		histValues[i] = 0;
	}
	//Calcul histograma
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
		histValues[lpSrc[i*w + j]] ++;
	//Calcul medie
	for (int i = 0; i < 256; i++)
	{
		myu = myu + i*histValues[i];
	}
	myu = myu / m;
	//Calcul deviatie
	for (int i = 0; i < 256; i++)
	{
		sigma = sigma + (i - myu)*(i - myu)*histValues[i];
	}
	sigma = sqrt(sigma / m);

	//Calcul histpdf
	for (int i = 0; i < 256; i++)
	{
		histpdf[i] = histValues[i] / m;
	}
	//Calcul histCpdf
	histCpdf[0] = histpdf[0];
	for (int i = 0; i < 255; i++)
	{
		histCpdf[i + 1] = histCpdf[i] + histpdf[i + 1];
	}

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		lpDst[i*w + j] = histCpdf[lpDst[i*w + j]] * 255;
	}

	END_PROCESSING("Egalizare histograma");
}

void convolutie(BYTE *lpSrc, int dwHeight, int dwWidth, int w, float *lpDst, float *nucleu, int kh, int kw)
{

	for (int i = kh/2; i < dwHeight - kh/2; i++)
		for (int j = kw/2; j < dwWidth - kw/2; j++)
		{
			float suma = 0;
			for (int ii = -kh / 2; ii <= kh / 2; ii++)
				for (int jj = -kw / 2; jj <= kw / 2; jj++)
				{
					suma = suma + lpSrc[(i + ii)*w + (j + jj)] * nucleu[(ii + kh / 2)*kw + (jj + kw / 2)];
				}
			lpDst[i*w + j] = suma;
		}
}


void CDibView::OnConvolutieMediearitmetica()
{
	BEGIN_PROCESSING();
	int s = 0;
	int nn = 5;
	float *nucleu = new float[nn*nn];
	for (int i = 0; i < nn; i++)
		for (int j = 0; j < nn; j++)
		{
			nucleu[i*nn + j] = 1;
			s += nucleu[i*nn + j];
		}
	float *temp = new float[w*dwHeight];
	memset(temp, 0.0f, sizeof(float));
	convolutie(lpSrc, dwHeight, dwWidth, w, temp, nucleu, nn, nn);
	for (int i = 0; i < dwHeight; i++)
	{
		for (int j = 0; j < dwWidth; j++)
		{
			lpDst[i*w + j] = temp[i*w + j] / s;
		}
	}
	END_PROCESSING("Convolutie cu nucleu artimetic");
}


void CDibView::OnConvolutieFiltrugaussian()
{
	BEGIN_PROCESSING();
	int s = 0;
	int nn = 3;
	float *nucleu = new float[nn*nn];
	int gaussian[3][3] = { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } };
	for (int i = 0; i < nn; i++)
		for (int j = 0; j < nn; j++)
		{
			nucleu[i*nn + j] = gaussian[i][j];
			s += nucleu[i*nn + j];
		}
	float *temp = new float[w*dwHeight];
	memset(temp, 0.0f, sizeof(float));
	convolutie(lpSrc, dwHeight, dwWidth, w, temp, nucleu, nn, nn);
	for (int i = 0; i < dwHeight; i++)
	{
		for (int j = 0; j < dwWidth; j++)
		{
			lpDst[i*w + j] = temp[i*w + j] / s;
		}
	}
	END_PROCESSING("Convolutie cu nucleu gaussian");
}


void CDibView::OnConvolutieFiltrulaplace()
{
	BEGIN_PROCESSING();
	int nn = 3;
	float sPlus = 0;
	float sMinus = 0;
	float s;
	float *nucleu = new float[nn*nn];
	int gaussian[3][3] = { { -1, -1, -1 }, { -1, 8, -1 }, { -1, -1, -1 } };
	for (int i = 0; i < nn; i++)
		for (int j = 0; j < nn; j++)
		{
			nucleu[i*nn + j] = gaussian[i][j];
			if (gaussian[i][j] >= 0)
			{
				sPlus += gaussian[i][j];
			}
			else
			{
				sMinus += -gaussian[i][j];
			}
		}
	s = 1 / (2*__max(sPlus, sMinus));

	float *lpTemp = new float[w*dwHeight];
	memset(lpTemp, 0.0f, sizeof(float));

	convolutie(lpSrc, dwHeight, dwWidth, w, lpTemp, nucleu, nn, nn);

	for (int i = 0; i < dwHeight; i++)
	{
		for (int j = 0; j < dwWidth; j++)
		{
			//lpDst[i*w + j] = s*lpTemp[i*w+j] + 128;
			lpDst[i*w + j] = __min(255, __max(0, lpTemp[i*w + j]));
		}
	}
	END_PROCESSING("Convolutie cu nucleu laplacian");
}


void CDibView::OnConvolutieFouriernochange()
{
	BEGIN_PROCESSING();
	double *real = new double[dwWidth*dwHeight];
	double *imag = new double[dwWidth*dwHeight];
	fftimage(dwWidth, dwHeight, lpSrc, (BYTE*)0, real, imag);
	ifftimage(dwWidth, dwHeight, real, imag, lpDst, (BYTE*)0);
	END_PROCESSING("Transformata Fourier si inversa");
}


void CDibView::OnConvolutieFouriertrece()
{

	BEGIN_PROCESSING();
	double *real = new double[dwWidth*dwHeight];
	double *imag = new double[dwWidth*dwHeight];
	double *modul = new double[dwWidth*dwHeight];
	BYTE *lpTemp = new BYTE[w*dwHeight];

	for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
		{
			lpTemp[i*w + j] = ((i + j) & 1) ? -lpSrc[i*w + j] : lpSrc[i*w + j];
		}
	fftimage(dwWidth, dwHeight, lpTemp, (BYTE*)0, real, imag);
	double max = 0;
	for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
		{
			if (((dwHeight / 2 - i)*(dwHeight / 2 - i) + (dwWidth / 2 - j)*(dwWidth / 2 - j)) > 9)
			{
				imag[i*dwWidth + j] = 0;
				real[i*dwWidth + j] = 0;
			}
			
		}

	ifftimage(dwWidth, dwHeight, real, imag, lpDst, (BYTE*)0);
	
	for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
		{
			lpDst[i*w + j] = ((i + j) & 1) ? -lpDst[i*w + j] : lpDst[i*w + j];;
		}
		
	END_PROCESSING("Transformata Fourier si inversa");
}


void CDibView::OnConvolutieFouriermodul()
{

	BEGIN_PROCESSING();
	double *real = new double[dwWidth*dwHeight];
	double *imag = new double[dwWidth*dwHeight];
	double *modul = new double[dwWidth*dwHeight];
	BYTE *lpTemp = new BYTE[w*dwHeight];

	for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
		{
			lpTemp[i*w + j] = ((i + j) & 1) ? -lpSrc[i*w + j] : lpSrc[i*w + j];
		}
	fftimage(dwWidth, dwHeight, lpTemp, (BYTE*)0, real, imag);
	double max = 0;
	for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
		{
			modul[i*dwWidth + j] = real[i*dwWidth + j] * real[i*dwWidth + j] + imag[i*dwWidth + j] * imag[i*dwWidth + j];
			modul[i*dwWidth + j] = sqrt(modul[i*dwWidth + j]);
			//comentata pentru imaginile sinusoidale (liniile alea)
			//modul[i*dwWidth + j] = log(modul[i*dwWidth + j]+1);
			if (max < modul[i*dwWidth + j])
			{
				max = modul[i*dwWidth + j];
			}
		}
	for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
		{
			lpDst[i*w + j] = modul[i*w + j] * (255 / max);
		}

	END_PROCESSING("Transformata Fourier modul");
}


void CDibView::OnConvolutieFouriermodulculog()
{

	BEGIN_PROCESSING();
	double *real = new double[dwWidth*dwHeight];
	double *imag = new double[dwWidth*dwHeight];
	double *modul = new double[dwWidth*dwHeight];
	BYTE *lpTemp = new BYTE[w*dwHeight];

	for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
		{
			lpTemp[i*w + j] = ((i + j) & 1) ? -lpSrc[i*w + j] : lpSrc[i*w + j];
		}
	fftimage(dwWidth, dwHeight, lpTemp, (BYTE*)0, real, imag);
	double max = 0;
	for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
		{
			modul[i*dwWidth + j] = real[i*dwWidth + j] * real[i*dwWidth + j] + imag[i*dwWidth + j] * imag[i*dwWidth + j];
			modul[i*dwWidth + j] = sqrt(modul[i*dwWidth + j]);
			//comentata pentru imaginile sinusoidale (liniile alea)
			modul[i*dwWidth + j] = log(modul[i*dwWidth + j] + 1);
			if (max < modul[i*dwWidth + j])
			{
				max = modul[i*dwWidth + j];
			}
		}
	for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
		{
			lpDst[i*w + j] = modul[i*w + j] * (255 / max);
		}

	END_PROCESSING("Transformata Fourier modul");
}


int* getSortedNeighbours(BYTE* lpSrc, int i, int j, int w, int n){
	int* neighbours = new int[9 * n];
	int k = 0, aux;
	for (int ii = n; ii >= -n; ii--)
		for (int jj = n; jj >= -n; jj--)
		{
			neighbours[k] = lpSrc[(i + ii)*w + (j + jj)];
			k++;
		}
	for (int ii = 0; ii < 9*n -1 ; ii++)
		for (int jj = ii; jj < 9 * n; jj++)
		{
			if (neighbours[ii]>neighbours[jj])
			{
				aux = neighbours[ii];
				neighbours[ii] = neighbours[jj];
				neighbours[jj] = aux;
			}
		}
	return neighbours;
}

void CDibView::OnFiltrareFiltrudeminim()
{
	int r = 1;
	int* neighbours = new int[9*r];
	BEGIN_PROCESSING();
	for (int i = r; i < dwHeight-r; i++)
		for (int j = r; j < dwWidth-r; j++)
		{
			neighbours = getSortedNeighbours(lpSrc, i, j, w, 1);
			lpDst[i*w + j] = neighbours[0];
		}
	END_PROCESSING("Filtru de minim");
}


void CDibView::OnFiltrareFiltrudemaxim()
{
	int r = 1;
	int* neighbours = new int[9 * r];
	BEGIN_PROCESSING();
	for (int i = r; i < dwHeight - r; i++)
		for (int j = r; j < dwWidth - r; j++)
		{
			neighbours = getSortedNeighbours(lpSrc, i, j, w, r);
			lpDst[i*w + j] = neighbours[9*r-1];
		}
	END_PROCESSING("Filtru de maxim");
}


void CDibView::OnFiltrareFiltrudemedie()
{	
	int r = 1;
	int media;
	int* neighbours = new int[9 * r];
	BEGIN_PROCESSING();
	for (int i = r; i < dwHeight - r; i++)
		for (int j = r; j < dwWidth - r; j++)
		{
			neighbours = getSortedNeighbours(lpSrc, i, j, w, r);			
			lpDst[i*w + j] = neighbours[5];
		}
	END_PROCESSING("Filtru de median");
}

// x linia
// y coloana
float calculGaussian(float sigma, int x, int y, int x0, int y0){
	float gauss;
	float fractie;
	float exponent;
	fractie = 1 / (2 * PI *sigma*sigma);
	exponent = -((x - x0)*(x - x0) + (y - y0)*(y - y0)) / (2 * sigma*sigma);
	gauss = fractie * exp(exponent);
	return gauss;
}

void CDibView::OnFiltrareFiltrugaussian()
{

	BEGIN_PROCESSING();
	float sigma = 0.8;
	int n;
	if (6*sigma - 6*(int)sigma > 0.5)
	{
		n = 6 * sigma + 1;
	}
	else
	{
		n = 6 * sigma;
	}

	float* gaussian = new float[n*n];
	for (int i = 0; i < n; i++)
		for (int j = 0; j < n; j++)
		{
			gaussian[i*n + j] = calculGaussian(sigma, i, j, n / 2, n / 2);
		}
	
	float* lpTemp = new float[dwHeight*w];
	convolutie(lpSrc, dwHeight, dwWidth, w, lpTemp, gaussian, n, n);

	for (int i = 0; i < dwHeight; i++)
		for (int j = 0; j < dwWidth; j++)
		{
			lpDst[i*w + j] = lpTemp[i*w + j];
		}
	END_PROCESSING("Filtrare cu Gaussian");
}

float calculGaussianUniDim(float sigma, int x, int x0){
	float gaussX;
	float fractie;
	float exponent;
	fractie = 1 / (sqrt(2 * PI) *sigma);
	exponent = -((x - x0)*(x - x0)) / (2 * sigma*sigma);
	gaussX = fractie * exp(exponent);
	return gaussX;
}

void filtrareGaussUniDim(BYTE *lpSrc, int dwHeight, int dwWidth, int w, BYTE *lpDst, float sigma){
	
	int n;
	if (6 * sigma - 6 * (int)sigma > 0.5)
	{
		n = 6 * sigma + 1;
	}
	else
	{
		n = 6 * sigma;
	}

	float* gaussianX = new float[n];
	for (int i = 0; i < n; i++)
		gaussianX[i] = calculGaussianUniDim(sigma, i, n / 2);
	float* gaussianY = new float[n];
	for (int i = 0; i < n; i++)
		gaussianY[i] = calculGaussianUniDim(sigma, i, n / 2);


	float* lpTemp1 = new float[dwHeight*w];
	convolutie(lpSrc, dwHeight, dwWidth, w, lpTemp1, gaussianX, n, 1);

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		lpDst[i*w + j] = lpTemp1[i*w + j];
	}

	float* lpTemp2 = new float[dwHeight*w];
	convolutie(lpDst, dwHeight, dwWidth, w, lpTemp2, gaussianY, 1, n);

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		lpDst[i*w + j] = lpTemp2[i*w + j];
	}
}

void gradientCuGaussianUniDim(BYTE *lpSrc, int dwHeight, int dwWidth, int w, float *gradient, float sigma){

	int n;
	if (6 * sigma - 6 * (int)sigma > 0.5)
	{
		n = 6 * sigma + 1;
	}
	else
	{
		n = 6 * sigma;
	}

	float* gaussianX = new float[n];
	for (int i = 0; i < n; i++)
		gaussianX[i] = calculGaussianUniDim(sigma, i, n / 2);
	float* gaussianY = new float[n];
	for (int i = 0; i < n; i++)
		gaussianY[i] = calculGaussianUniDim(sigma, i, n / 2);


	float* lpTempX = new float[dwHeight*w];
	convolutie(lpSrc, dwHeight, dwWidth, w, lpTempX, gaussianX, n, 1);
	
	float* lpTempY = new float[dwHeight*w];
	convolutie(lpSrc, dwHeight, dwWidth, w, lpTempY, gaussianY, 1, n);

	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		gradient[i*w + j] = sqrt(lpTempX[i*w + j] * lpTempX[i*w + j] + lpTempY[i*w + j] * lpTempY[i*w + j]);
	}
}

void CDibView::OnFiltrareFiltrugaussianseparat()
{
	
	BEGIN_PROCESSING();
	float sigma = 0.8;
	filtrareGaussUniDim(lpSrc, dwHeight, dwWidth, w, lpDst,sigma);
	END_PROCESSING("Filtrare cu Gaussian");
}


void cannyAlg(BYTE *lpSrc, int dwHeight, int dwWidth, int w, BYTE *lpDst){
	int xArray[3][3] = { { -1, 0, 1 }, { -2, 0, 2 }, { -1, 0, 1 } };
	int nn = 3;
	float *nucleu = new float[nn*nn];
	for (int i = 0; i < nn; i++)
	for (int j = 0; j < nn; j++)
		nucleu[i*nn + j] = xArray[i][j];
	float *gradfx = new float[w*dwHeight];
	convolutie(lpSrc, dwHeight, dwWidth, w, gradfx, nucleu, nn, nn);


	int yArray[3][3] = { { 1, 2, 1 }, { 0, 0, 0 }, { -1, -2, -1 } };

	for (int i = 0; i < nn; i++)
	for (int j = 0; j < nn; j++)
		nucleu[i*nn + j] = yArray[i][j];
	float *gradfy = new float[w*dwHeight];
	convolutie(lpSrc, dwHeight, dwWidth, w, gradfy, nucleu, nn, nn);

	float *modulGradient = new float[w*dwHeight];
	float *directie = new float[w*dwHeight];
	float degrees;
	int *dir = new int[w*dwHeight];
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		modulGradient[i*w + j] = sqrt((gradfx[i*w + j] * gradfx[i*w + j]) + (gradfy[i*w + j] * gradfy[i*w + j]));
		directie[i*w + j] = atan2(gradfy[i*w + j], gradfx[i*w + j]);

		degrees = directie[i*w + j] * 180 / PI;
		if ((degrees <= 22.5 || degrees >= 360 - 22.5) || (degrees >= 180 - 22.5 && degrees <= 180 + 22.5))
			dir[i*w + j] = 2;
		else
		if ((degrees <= 45 + 22.5 && degrees >= 45 - 22.5) || (degrees >= 225 - 22.5 && degrees <= 225 + 22.5))
			dir[i*w + j] = 1;
		else
		if ((degrees >= 135 - 22.5 && degrees <= 135 + 22.5) || (degrees >= 315 - 22.5 && degrees <= 315 + 22.5))
			dir[i*w + j] = 3;
		else
		if ((degrees >= 90 - 22.5 && degrees <= 90 + 22.5) || (degrees >= 270 - 22.5 && degrees <= 270 + 22.5))
			dir[i*w + j] = 0;
	}
	for (int i = 1; i < dwHeight - 1; i++)
	for (int j = 1; j < dwWidth - 1; j++)
	{
		float P = modulGradient[i*w + j];
		switch (dir[i*w + j]){
		case 0:
		{
				  if (P < modulGradient[(i + 1)*w + j] || P < modulGradient[(i - 1)*w + j])
					  modulGradient[i*w + j] = 0;
		}
			break;
		case 1:
		{
				  if (P < modulGradient[(i + 1)*w + j - 1] || P < modulGradient[(i - 1)*w + j + 1])

					  modulGradient[i*w + j] = 0;
		}
			break;
		case 2:
		{
				  if (P<modulGradient[i*w + j - 1] || P<modulGradient[i*w + j + 1])

					  modulGradient[i*w + j] = 0;
		}
			break;
		case 3:
		{
				  if (P<modulGradient[(i - 1)*w + j - 1] || P<modulGradient[(i + 1)*w + j + 1])

					  modulGradient[i*w + j] = 0;
		}
			break;
		}

	}
	// lpTemp = imaginea gradientului		
	BYTE* lpTemp = new BYTE[w*dwHeight];

	for (int i = 1; i < dwHeight - 1; i++)
	for (int j = 1; j < dwWidth - 1; j++)
	if (modulGradient[i*w + j] / 4>255)
		lpTemp[i*w + j] = 255;
	else
		lpTemp[i*w + j] = modulGradient[i*w + j] / 4;

	//initializare histograma
	int histGrad[256];
	for (int i = 0; i <= 255; i++)
		histGrad[i] = 0;
	//calculare histograma
	for (int i = 1; i < dwHeight - 1; i++)
	for (int j = 1; j < dwWidth - 1; j++)
	{
		histGrad[lpTemp[i*w + j]]++;
	}

	int NNM;
	float p = 0.1;
	NNM = (1 - p)*(dwWidth*dwHeight - histGrad[0]);

	//gasire prag T Hight
	// argumentul minim pentru care nr de pixeli adunati >= NNM
	int TH = 0;
	int histSum = 0;
	for (int i = 1; i <= 255; i++)
	{
		histSum += histGrad[i];
		if (histSum >= NNM)
		{
			TH = i;
			break;
		}
	}
	//calulare T Low
	int TL = 0.4 * TH;

	for (int i = 1; i < dwHeight - 1; i++)
	for (int j = 1; j < dwWidth - 1; j++)
	if (lpTemp[i*w + j] >= TH)
		lpTemp[i*w + j] = 255;
	else
	{
		if (lpTemp[i*w + j] < TL)
			lpTemp[i*w + j] = 0;
		else
			lpTemp[i*w + j] = 128;
	}
	//etichetare imagine muchii slabe in muchii tari
	int ok = 0;
	bool schimbare = true;
	while (schimbare)
	{
		schimbare = false;

		for (int i = 4; i < dwHeight - 4; i++)
		for (int j = 4; j < dwWidth - 4; j++)
		if (lpTemp[i*w + j] == 128)
		{
			/* vecinatate de 8
			if (lpTemp[(i - 1)*w + j] == 255 ||
			lpTemp[(i + 1)*w + j] == 255 ||
			lpTemp[i*w + (j - 1)] == 255 ||
			lpTemp[i*w + (j + 1)] == 255 ||
			lpTemp[(i + 1)*w + (j + 1)] == 255 ||
			lpTemp[(i - 1)*w + (j - 1)] == 255 ||
			lpTemp[(i + 1)*w + (j - 1)] == 255 ||
			lpTemp[(i - 1)*w + (j + 1)] == 255)
			*/
			ok = 0;
			for (int ii = i - 3; ii <= i + 3; ii++)
			for (int jj = j - 3; jj <= j + 3; jj++)
			if (lpTemp[ii*w + jj] == 255)
			{
				ok = 1;
			}
			if (ok == 1)
			{
				lpTemp[i*w + j] = 255;
				schimbare = true;
			}
		}
	}

	for (int i = 1; i < dwHeight - 1; i++)
	for (int j = 1; j < dwWidth - 1; j++)
	if (lpTemp[i*w + j] == 128)
		lpTemp[i*w + j] = 0;

	for (int i = 1; i < dwHeight - 1; i++)
	for (int j = 1; j < dwWidth - 1; j++)
		lpDst[i*w + j] = lpTemp[i*w + j];
}

void CDibView::OnEdgedetectionCanny()
{
	BEGIN_PROCESSING();
	cannyAlg(lpSrc, dwHeight, dwWidth, w, lpDst);
	END_PROCESSING("Canny");
	

}

void gradientPrewitt(BYTE *lpBlured, int dwHeight, int dwWidth, int w, float *modulGradient){
	int nn = 3;
	float *nucleu = new float[nn*nn];
	int xArray[3][3] = { { -1, 0, 1 }, { -1, 0, 1 }, { -1, 0, 1 } };
	for (int i = 0; i < nn; i++)
	for (int j = 0; j < nn; j++)
		nucleu[i*nn + j] = xArray[i][j];
	float *gradfx = new float[w*dwHeight];
	convolutie(lpBlured, dwHeight, dwWidth, w, gradfx, nucleu, nn, nn);

	int yArray[3][3] = { { -1, -1, -1 }, { 0, 0, 0 }, { 1, 1, 1 } };
	for (int i = 0; i < nn; i++)
	for (int j = 0; j < nn; j++)
		nucleu[i*nn + j] = yArray[i][j];
	float *gradfy = new float[w*dwHeight];
	convolutie(lpBlured, dwHeight, dwWidth, w, gradfy, nucleu, nn, nn);

	
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		modulGradient[i*w + j] = sqrt((gradfx[i*w + j] * gradfx[i*w + j]) + (gradfy[i*w + j] * gradfy[i*w + j]));
	}
}

void CDibView::OnProiectGaussianratio()
{
	BEGIN_PROCESSING();
	int muchie = 255;
	int fundal = 0;
	//pas 1 detectie muchii
	BYTE *lpMuchii = new BYTE[w*dwHeight];
	cannyAlg(lpSrc, dwHeight, dwWidth, w, lpMuchii);

	//pas 2 reblure edges with gaussian 
	// facut cu calulul gaussianului in functie de sigma (laboratorul cu zgomot)
	// sigma = 1
	float sigma0 = 0.8;
	BYTE *lpBlured = new BYTE[w*dwHeight];
	BYTE *lpReblured = new BYTE[w*dwHeight];
	filtrareGaussUniDim(lpSrc, dwHeight, dwWidth, w, lpBlured, sigma0);
	filtrareGaussUniDim(lpBlured, dwHeight, dwWidth, w, lpReblured,sigma0);

	//pas 3 gradientul imaginilor
	// facut cu conv cu nuclee prewitt
	float *modulBlured = new float[w*dwHeight];
	float *modulReblured = new float[w*dwHeight];
	float *modulSrc = new float[w*dwHeight];
	gradientPrewitt(lpSrc, dwHeight, dwWidth, w, modulSrc);
	gradientPrewitt(lpBlured, dwHeight, dwWidth, w, modulBlured);
	gradientPrewitt(lpReblured, dwHeight, dwWidth, w, modulReblured);

	//pas 4 raport intre modulele gradientelor imaginii blurate si imaginii reblurate 
	float *ratio = new float[w*dwHeight];
	float R = 0;
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (lpMuchii[i*w + j] == muchie)
		{
			ratio[i*w + j] = modulBlured[i*w + j] / modulReblured[i*w + j];
			//ratio[i*w + j] = modulSrc[i*w + j] / modulBlured[i*w + j];
			//ratio[i*w + j] = modulSrc[i*w + j] / modulReblured[i*w + j];
		}
		else
		{
			ratio[i*w + j] = -1;
		}
		R = __max(R, ratio[i*w + j]);
	}

	//pas 5 calculare blur amount dupa sigma folosita la calcularea gaussianului 
	float *blurAmount = new float[w*dwHeight];
	float B = 0;
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (lpMuchii[i*w + j] == muchie)
		{
			blurAmount[i*w + j] = (1 / sqrt(ratio[i*w + j] * ratio[i*w + j] - 1)) * sigma0;
		}
		else
		{
			blurAmount[i*w + j] = 0;
		}
		B = __max(B, blurAmount[i*w + j]);
	}
	//pas 6 folosirea noii valori sigma(blur amount pentru convolutie gaussiana)
	BYTE *lpSmap = new BYTE[w*dwHeight];
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (blurAmount[i*w+j]>0)
		{
			lpSmap[i*w + j] = ratio[i*w + j] * 255 / R;
		}
		else
			lpSmap[i*w + j] = 0;

		lpDst[i*w + j] =blurAmount[i*w+j]*100;
		//lpDst[i*w + j] = lpMuchii[i*w + j];
	}


	//convolutie gaussiana cu sigma personalizat pentru fiecare pixel
	CString info;
	info.Format(_TEXT("R= %f %f %f %f"), blurAmount[12*w+14], blurAmount[12*w+15], blurAmount[12*w+16], blurAmount[100*w+100]);
	AfxMessageBox(info);
	END_PROCESSING("Canny");
}


void CDibView::OnProiectLpsrc()
{
	BEGIN_PROCESSING();
	int muchie = 255;
	int fundal = 0;
	//pas 1 detectie muchii
	BYTE *lpMuchii = new BYTE[w*dwHeight];
	cannyAlg(lpSrc, dwHeight, dwWidth, w, lpMuchii);

	//pas 2 reblure edges with gaussian 
	// facut cu calulul gaussianului in functie de sigma (laboratorul cu zgomot)
	// sigma = 1
	float sigma0 = 0.8;
	BYTE *lpBlured = new BYTE[w*dwHeight];
	BYTE *lpReblured = new BYTE[w*dwHeight];
	filtrareGaussUniDim(lpSrc, dwHeight, dwWidth, w, lpBlured, sigma0);
	filtrareGaussUniDim(lpBlured, dwHeight, dwWidth, w, lpReblured, sigma0);

	//pas 3 gradientul imaginilor
	// facut cu conv cu nuclee prewitt
	float *modulBlured = new float[w*dwHeight];
	float *modulReblured = new float[w*dwHeight];
	float *modulSrc = new float[w*dwHeight];
	gradientPrewitt(lpSrc, dwHeight, dwWidth, w, modulSrc);
	gradientPrewitt(lpBlured, dwHeight, dwWidth, w, modulBlured);
	gradientPrewitt(lpReblured, dwHeight, dwWidth, w, modulReblured);
	//gradientCuGaussianUniDim(lpSrc, dwHeight, dwWidth, w, modulSrc, sigma0);
	//gradientCuGaussianUniDim(lpBlured, dwHeight, dwWidth, w, modulBlured, sigma0);
	//gradientCuGaussianUniDim(lpReblured, dwHeight, dwWidth, w, modulReblured, sigma0);

	//pas 4 raport intre modulele gradientelor imaginii blurate si imaginii reblurate 
	float *ratio = new float[w*dwHeight];
	float R = 0;
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (lpMuchii[i*w + j] == muchie)
		{
			ratio[i*w + j] = modulSrc[i*w + j] / modulBlured[i*w + j];
			//ratio[i*w + j] = modulBlured[i*w + j] / modulReblured[i*w + j];
			//ratio[i*w + j] = modulSrc[i*w + j] / modulReblured[i*w + j];
		}
		else
		{
			ratio[i*w + j] = -1;
		}
		R = __max(R, ratio[i*w + j]);
	}


	//pas 5 calculare blur amount dupa sigma folosita la calcularea gaussianului 
	float *blurAmount = new float[w*dwHeight];
	float Bmax = 0;
	float Bmin = 99999;
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (lpMuchii[i*w + j] == muchie)
		{
			blurAmount[i*w + j] = (1 / sqrt(ratio[i*w + j] * ratio[i*w + j] - 1)) * sigma0;
			Bmax = __max(Bmax, blurAmount[i*w + j]);
			Bmin = __min(Bmin, blurAmount[i*w + j]);
		}
		else
		{
			blurAmount[i*w + j] = 0;
		}
	}

	//pas 6 folosirea noii valori sigma pe interval 50 - 250
	float interval = (Bmax - Bmin) / 3;
	BYTE *lpSmap = new BYTE[w*dwHeight];
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		float b = blurAmount[i*w + j];
		if (b>0)
		{
			if (b>Bmin && b <= Bmin + interval)
			{
				lpSmap[i*w + j] = 50;
			}
			else
			{
				if (b > Bmin + interval && b <= Bmin + 2 * interval)
				{
					lpSmap[i*w + j] = 150;
				}
				else
				{
					lpSmap[i*w + j] = 250;
				}
			}
		}
		else
			lpSmap[i*w + j] = 0;

		lpDst[i*w + j] = lpSmap[i*w + j];
		lpDst[i*w + j] = blurAmount[i*w + j] * 255 / Bmax;
	}
	END_PROCESSING("lpSrc/lpBlurred");
}


void CDibView::OnProiectLpblured()
{
	BEGIN_PROCESSING();
	int muchie = 255;
	int fundal = 0;
	//pas 1 detectie muchii
	BYTE *lpMuchii = new BYTE[w*dwHeight];
	cannyAlg(lpSrc, dwHeight, dwWidth, w, lpMuchii);

	//pas 2 reblure edges with gaussian 
	// facut cu calulul gaussianului in functie de sigma (laboratorul cu zgomot)
	// sigma = 1
	float sigma0 = 0.8;
	BYTE *lpBlured = new BYTE[w*dwHeight];
	BYTE *lpReblured = new BYTE[w*dwHeight];
	filtrareGaussUniDim(lpSrc, dwHeight, dwWidth, w, lpBlured, sigma0);
	filtrareGaussUniDim(lpBlured, dwHeight, dwWidth, w, lpReblured, sigma0);

	//pas 3 gradientul imaginilor
	// facut cu conv cu nuclee prewitt
	float *modulBlured = new float[w*dwHeight];
	float *modulReblured = new float[w*dwHeight];
	float *modulSrc = new float[w*dwHeight];
	gradientPrewitt(lpSrc, dwHeight, dwWidth, w, modulSrc);
	gradientPrewitt(lpBlured, dwHeight, dwWidth, w, modulBlured);
	gradientPrewitt(lpReblured, dwHeight, dwWidth, w, modulReblured);
	//gradientCuGaussianUniDim(lpSrc, dwHeight, dwWidth, w, modulSrc, sigma0);
	//gradientCuGaussianUniDim(lpBlured, dwHeight, dwWidth, w, modulBlured, sigma0);
	//gradientCuGaussianUniDim(lpReblured, dwHeight, dwWidth, w, modulReblured, sigma0);


	//pas 4 raport intre modulele gradientelor imaginii blurate si imaginii reblurate 
	float *ratio = new float[w*dwHeight];
	float R = 0;
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (lpMuchii[i*w + j] == muchie)
		{
			//ratio[i*w + j] = modulSrc[i*w + j] / modulBlured[i*w + j];
			ratio[i*w + j] = modulBlured[i*w + j] / modulReblured[i*w + j];			
			//ratio[i*w + j] = modulSrc[i*w + j] / modulReblured[i*w + j];
		}
		else
		{
			ratio[i*w + j] = -1;
		}
		R = __max(R, ratio[i*w + j]);
	}

	//pas 5 calculare blur amount dupa sigma folosita la calcularea gaussianului 
	float *blurAmount = new float[w*dwHeight];
	float Bmax = 0;
	float Bmin = 99999;
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (lpMuchii[i*w + j] == muchie)
		{
			blurAmount[i*w + j] = (1 / sqrt(ratio[i*w + j] * ratio[i*w + j] - 1)) * sigma0;
			Bmax = __max(Bmax, blurAmount[i*w + j]);
			Bmin = __min(Bmin, blurAmount[i*w + j]);
		}
		else
		{
			blurAmount[i*w + j] = 0;
		}
	}
	
	//pas 6 folosirea noii valori sigma pe interval 50 - 250
	float interval = (Bmax - Bmin) / 3;
	BYTE *lpSmap = new BYTE[w*dwHeight];
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		float b = blurAmount[i*w + j];
		if (b>0)
		{
			if (b>Bmin && b <= Bmin + interval)
			{
				lpSmap[i*w + j] = 50;
			}
			else
			{
				if (b > Bmin + interval && b <= Bmin + 2 * interval)
				{
					lpSmap[i*w + j] = 150;
				}
				else
				{
					lpSmap[i*w + j] = 250;
				}
			}
		}
		else
			lpSmap[i*w + j] = 0;

		//lpDst[i*w + j] = lpSmap[i*w + j];
		lpDst[i*w + j] = blurAmount[i*w + j] * 255 / Bmax;
	}
	END_PROCESSING("lpBlurred/lpReblured");
}


void CDibView::OnProiectLpsrc32807()
{
	BEGIN_PROCESSING();
	int muchie = 255;
	int fundal = 0;
	//pas 1 detectie muchii
	BYTE *lpMuchii = new BYTE[w*dwHeight];
	cannyAlg(lpSrc, dwHeight, dwWidth, w, lpMuchii);

	//pas 2 reblure edges with gaussian 
	// facut cu calulul gaussianului in functie de sigma (laboratorul cu zgomot)
	// sigma = 1
	float sigma0 = 0.8;
	BYTE *lpBlured = new BYTE[w*dwHeight];
	BYTE *lpReblured = new BYTE[w*dwHeight];
	filtrareGaussUniDim(lpSrc, dwHeight, dwWidth, w, lpBlured, sigma0);
	filtrareGaussUniDim(lpBlured, dwHeight, dwWidth, w, lpReblured, sigma0);

	//pas 3 gradientul imaginilor
	// facut cu conv cu nuclee prewitt
	float *modulBlured = new float[w*dwHeight];
	float *modulReblured = new float[w*dwHeight];
	float *modulSrc = new float[w*dwHeight];
	gradientPrewitt(lpSrc, dwHeight, dwWidth, w, modulSrc);
	gradientPrewitt(lpBlured, dwHeight, dwWidth, w, modulBlured);
	gradientPrewitt(lpReblured, dwHeight, dwWidth, w, modulReblured);
	//gradientCuGaussianUniDim(lpSrc, dwHeight, dwWidth, w, modulSrc, sigma0);
	//gradientCuGaussianUniDim(lpBlured, dwHeight, dwWidth, w, modulBlured, sigma0);
	//gradientCuGaussianUniDim(lpReblured, dwHeight, dwWidth, w, modulReblured, sigma0);

	//pas 4 raport intre modulele gradientelor imaginii blurate si imaginii reblurate 
	float *ratio = new float[w*dwHeight];
	float R = 0;
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (lpMuchii[i*w + j] == muchie)
		{
			//ratio[i*w + j] = modulSrc[i*w + j] / modulBlured[i*w + j];
			//ratio[i*w + j] = modulBlured[i*w + j] / modulReblured[i*w + j];
			ratio[i*w + j] = modulSrc[i*w + j] / modulReblured[i*w + j];
		}
		else
		{
			ratio[i*w + j] = -1;
		}
		R = __max(R, ratio[i*w + j]);
	}

	//pas 5 calculare blur amount dupa sigma folosita la calcularea gaussianului 
	float *blurAmount = new float[w*dwHeight];
	float Bmax = 0;
	float Bmin = 100000;
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		if (lpMuchii[i*w + j] == muchie)
		{
			blurAmount[i*w + j] = (1 / sqrt(ratio[i*w + j] * ratio[i*w + j] - 1)) * sigma0;
			Bmax = __max(Bmax, blurAmount[i*w + j]);
			Bmin = __min(Bmin, blurAmount[i*w + j]);
		}
		else
		{
			blurAmount[i*w + j] = 0;
		}
	}

	//pas 6 folosirea noii valori sigma pe interval 50 - 250
	float interval = (Bmax - Bmin) / 3;
	BYTE *lpSmap = new BYTE[w*dwHeight];
	for (int i = 0; i < dwHeight; i++)
	for (int j = 0; j < dwWidth; j++)
	{
		/*
		float b = blurAmount[i*w + j];
		if (b>0)
		{
			if (b>Bmin && b <= Bmin + interval)
			{
				lpSmap[i*w + j] = 50;
			}
			else
			{
				if (b > Bmin + interval && b <= Bmin + 2 * interval)
				{
					lpSmap[i*w + j] = 150;
				}
				else
				{
					lpSmap[i*w + j] = 250;
				}
			}
		}
		else
			lpSmap[i*w + j] = 0;
			*/

		//lpDst[i*w + j] = lpSmap[i*w + j];

		lpDst[i*w + j] = blurAmount[i*w + j] * 255 / Bmax;
	}

	
	END_PROCESSING("lpSrc/lpReblured");
}


void CDibView::OnSscHexainfisier()
{
	FILE *pFile;
	pFile = fopen("imagine.txt", "w");
	BEGIN_PROCESSING();

	for (int i = 0; i < 8; i++)
	{
		for (int j = 0; j < 8; j++)
		{
			fprintf(pFile, "x\"%x\", ", lpSrc[i*w + j]);
			lpDst[i*w + j] = __max(255, lpSrc[i*w + j] + 80);
		}
	}
	fclose(pFile);
	END_PROCESSING("Pus �n fisier");
}
