// Histogram.cpp : implementation file
//

#include "stdafx.h"
#include "diblook.h"
#include "Histogram.h"


// CHistogram

IMPLEMENT_DYNAMIC(CHistogram, CStatic)

CHistogram::CHistogram()
{

}

CHistogram::~CHistogram()
{
}


BEGIN_MESSAGE_MAP(CHistogram, CStatic)
	ON_WM_PAINT()
END_MESSAGE_MAP()



// CHistogram message handlers




void CHistogram::OnPaint()
{
	CPaintDC dc(this); // device context for painting
	// TODO: Add your message handler code here

	CPen pen(PS_SOLID, 1, RGB(255, 0, 0));
	CPen *pTempPen = dc.SelectObject(&pen);
	CRect rect;
	GetClientRect(rect);
	int height = rect.Height();
	int width = rect.Width();

	int i;
	int maxValue = 0;
	for (i = 0; i < 256; i++)
	if (values[i]>maxValue)
		maxValue = values[i];

	double scaleFactor = 1.0;
	if (maxValue >= height)
	{
		scaleFactor = (double)height / maxValue;
	}

	for (i = 0; i < 256; i++)
	{
		int lengthLine = (int)(scaleFactor*values[i]);

		dc.MoveTo(i, height);
		dc.LineTo(i, height - lengthLine);
	}

	dc.SelectObject(pTempPen);
	// Do not call CStatic::OnPaint() for painting messages

}
