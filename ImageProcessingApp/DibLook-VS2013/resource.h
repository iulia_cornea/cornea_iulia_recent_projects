//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by diblook.rc
//
#define IDR_MAINFRAME                   2
#define IDR_DIBTYPE                     3
#define IDS_DIB_TOO_BIG                 4
#define IDS_CANNOT_LOAD_DIB             5
#define IDS_CANNOT_SAVE_DIB             6
#define IDD_ABOUTBOX                    100
#define IDC_EDIT1                       102
#define ID_BMP_ver                      103
#define IDD_DIALOG1                     104
#define ID_HeaderSize                   104
#define ID_BitsPixel                    105
#define IDD_DIALOG2                     105
#define ID_Width                        106
#define ID_Height                       107
#define IDD_DIALOG3                     107
#define ID_LUTSize                      108
#define ID_LUTContent                   109
#define IDC_HISTOGRAM                   111
#define ID_PROCESSING_DUPLICATE         32770
#define ID_PROCESSING_PARCURGERESIMPLA  32771
#define ID_PROCESSING_MULTIPLETHRESHOLDING 32772
#define ID_PROCESSING_PSEUDOCOLORARE    32773
#define ID_PROCESSING_AFISAREBITMAPINFO 32774
#define ID_PROCESSING_GRAYSCALE         32775
#define ID_PROCESSING_BINARIZARECUPRAG  32776
#define ID_PROCESSING_HISTOGRAMA        32777
#define ID_PROCESSING_REDUCERENIVELURIDEGRI 32778
#define ID_PROCESSING_ETICHETARE        32779
#define ID_PROCESSING_PARCURGERECONTUR  32780
#define ID_PROCESSING_RECONSTRUCTIECONTUR 32781
#define ID_PROCESSING_DILATARE          32782
#define ID_PROCESSING_E                 32783
#define ID_PROCESSING_DESCHIDERE        32784
#define ID_PROCESSING_EROZIUNE          32785
#define ID_PROCESSING_INCHIDERE         32786
#define ID_PROCESSING_EXTRAGERECONTUR   32787
#define ID_PROCESSING_BINARIZARECUPRAGAUTOMAT 32788
#define ID_PROCESSING_HISTOGRAMALUMINOZITATE 32789
#define ID_PROCESSING_EGALIZAREHISTOGRAMA 32790
#define ID_CONVOLUTIE_MEDIEARITMETICA   32791
#define ID_CONVOLUTIE_FILTRUGAUSSIAN    32792
#define ID_CONVOLUTIE_FILTRULAPLACE     32793
#define ID_CONVOLUTIE_FOURIERNOCHANGE   32794
#define ID_CONVOLUTIE_FOURIERTRECE      32795
#define ID_CONVOLUTIE_FOURIERMODUL      32796
#define ID_CONVOLUTIE_FOURIERMODULCULOG 32797
#define ID_FILTRARE_FILTRUDEMINIM       32798
#define ID_FILTRARE_FILTRUDEMAXIM       32799
#define ID_FILTRARE_FILTRUDEMEDIE       32800
#define ID_FILTRARE_FILTRUGAUSSIAN      32801
#define ID_FILTRARE_FILTRUGAUSSIANSEPARAT 32802
#define ID_EDGEDETECTION_CANNY          32803
#define ID_PROIECT_GAUSSIANRATIO        32804
#define ID_PROIECT_LPSRC                32805
#define ID_PROIECT_LPBLURED             32806
#define ID_PROIECT_LPSRC32807           32807
#define ID_SSC_HEXAINFISIER             32808

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        108
#define _APS_NEXT_COMMAND_VALUE         32809
#define _APS_NEXT_CONTROL_VALUE         112
#define _APS_NEXT_SYMED_VALUE           102
#endif
#endif
